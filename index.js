// Created by FearLixY

// import library
var express = require('express');
var mysql = require('mysql');
var dateTime = require('node-datetime');
var bodyParser = require('body-parser');
var crypto = require('crypto');
var hat = require('hat');
var bodyParser = require('body-parser');

// initialise
var app = express();
var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "G4ptun;-rJOG",
  database: "FYP"
});

con.connect(function(err) {
	if (err) throw err;
	var dt = dateTime.create();
	var formattedDt = dt.format('Y-m-d H:M:S');
	console.log("MySQL Connected!" + formattedDt);
});

// Config
var port = process.env.PORT || 3000;

// start the server
app.listen(port);

app.get('/', function(req, res) {
	res.send('Server Connected, the API is at port ' + port + '(/api)');
});

// get an instance of the router for api routes
var appApi = express.Router();
app.use('/api', appApi);

//parse raw body
app.use(bodyParser.raw());

// APIs
appApi.get('/test', function(req, res) {
	res.json({SUCCESS: true});
});

appApi.get('/login', function(req, res) {
	var email = req.header("email");
	var password = req.header("password");
	var iosToken = req.header("iostoken");
	var androidToken = req.header("androidtoken");
	var passwordHash = password.hash();
	var token = (email + "SAMMINGONCE&ALWAYS" + hat()).shuffle();
	var tokenHash = token.hash();
	var sql = 'SELECT USERID, ISCHINESE FROM MEMBER WHERE EMAIL=? AND UPPER(PASSWORD)=?';
	con.query(sql, [email, passwordHash], function(err, result) {
		if(err) throw err;
		if(result[0] != null) {
			res.json({
				"type": "M",
				"id": result[0].USERID,
				"isChinese": result[0].ISCHINESE,
				"token": token
			});
			if(iosToken != null) {
				con.query('UPDATE MEMBER SET ANDROIDTOKEN="", TOKENHASH=?, IOSTOKEN=? WHERE USERID=?', [tokenHash, iosToken, result[0].USERID]);
			} else if(androidToken != null) {
				con.query('UPDATE MEMBER SET IOSTOKEN="", TOKENHASH=?, ANDROIDTOKEN=? WHERE USERID=?', [tokenHash, androidToken, result[0].USERID]);
			}
			return;
		} else {
			sql = 'SELECT STAFFID, CENTERBRANCHID, ISCHINESE FROM STAFF WHERE EMAIL=? AND UPPER(PASSWORD)=?';
			con.query(sql, [email, passwordHash], function(err, result) {
				if(err) throw err;
				if(result[0] != null && result[0].BRANCHID != 0) {
					res.json({
						"type": "S",
						"id": result[0].STAFFID,
						"isChinese": result[0].ISCHINESE,
						"token": token
					});
					if(iosToken != null) {
						con.query('UPDATE STAFF SET TOKENHASH=?, IOSTOKEN=? WHERE STAFFID=?', [tokenHash, iosToken, result[0].STAFFID]);
					} else if(androidToken != null) {
						con.query('UPDATE STAFF SET TOKENHASH=?, ANDROIDTOKEN=? WHERE STAFFID=?', [tokenHash, androidToken, result[0].STAFFID]);
					}
					return;
				} else {
					res.json([]);
					return;
				}
			});
		}
	});
});
appApi.get('/getbookbyid', function(req, res) {
	var id = req.header("id");
	var sql = 'SELECT B.*, T1.RESERVEDUSERID, T1.RESERVATIONENDDATE, T2.BORROWEDUSERID, T3.CENTERID FROM BOOK B LEFT JOIN ( SELECT R.BOOKID, R.USERID AS RESERVEDUSERID, R.ENDDATE AS RESERVATIONENDDATE FROM RESERVATION R ORDER BY RESERVATIONENDDATE DESC ) AS T1 ON T1.BOOKID=B.BOOKID LEFT JOIN ( SELECT BB.BOOKID, BB.USERID AS BORROWEDUSERID FROM BORROWSBOOK BB WHERE BB.RETURNDATE IS NULL ) AS T2 ON T2.BOOKID=B.BOOKID LEFT JOIN ( SELECT CB.CENTERID, B2.BOOKID FROM CENTERBRANCH CB, BOOK B2 WHERE CB.CENTERBRANCHID=B2.CENTERBRANCHID ) AS T3 ON T3.BOOKID=B.BOOKID WHERE B.BOOKID=? LIMIT 1';
	con.query(sql, [id], function(err, result) {
		if(err) throw err;
		if(result[0] != null) {
			res.json(
				{
					"bookID": result[0].BookID,
		            "centerBranchID": result[0].CenterBranchID,
		            "name": result[0].BookName,
		            "desription": result[0].BookDesription,
		            "dewey": result[0].Dewey,
		            "author": result[0].Author,
		            "iSBN": result[0].ISBN,
		            "publishDate": result[0].PublishDate,
		            "entryDate": result[0].EntryDate,
		            "iCON": result[0].ICON,
		            "quantityERROR": result[0].NumberOfBook,
		            "position": result[0].BookPosition,
		            "ageLimit": result[0].AgeLimit,
		            "noBorrow": result[0].NotBorrows,
		            "notReservation": result[0].NotReservation,
		            "reservedUserId": result[0].RESERVEDUSERID,
		            "reservationEndDate": result[0].RESERVATIONENDDATE,
		            "borrowedUserId": result[0].BORROWEDUSERID,
		            "centerId": result[0].CENTERID
			    }
			);
			return;
		} else {
			res.json([]);
			return;
		}
	});
});
appApi.get('/getmemberbyid', function(req, res) {
	var id = req.header("id");
	var output = {}
	var sql = 'SELECT * FROM MEMBER WHERE USERID=?';
	con.query(sql, [id], function(err, result) {
		if(err) throw err;
		if(result[0] != null) {
			output = {
				"userId": result[0].UserID,
	            "centerId": result[0].CenterID,
	            "firstName": result[0].FirstName,
	            "lastName": result[0].LastName,
	            "email": result[0].Email,
	            "hKId": result[0].HKID,
	            "dateOfBirth": result[0].DateOfBirth,
	            "phone": result[0].Phone,
	            "regDate": result[0].RegDate,
	            "validTime": result[0].VaildTime,
	            "validCode": result[0].ValidCode,
	            "validState": result[0].VaildState,
	            "loginTime": result[0].LoginTime,
	            "logoutTime": result[0].LogoutTime,
	            "errorTime": result[0].ErrorTime,
	            "point": result[0].Point,
	            "icon": result[0].ICON,
	            "reservations": []
		    };
		    con.query("SELECT R.RESERVATIONID, R.ISBN, R.USERID, R.ENDDATE, R.VALID, B.BOOKNAME, B.AUTHOR, B.ICON FROM RESERVATION R, BOOK B WHERE VALID=1 AND B.ISBN=R.ISBN AND USERID=?", [result[0].UserID], function(err, result) {
		    	// output[0].reservations.push({
		    	// 	"iSBN": result[0].ISBN
		    	// });
		    	// if (result.length != 0 && result[0].VALID == 1 && new Date(result[0].ENDDATE).withoutTime() >= new Date().withoutTime()) {
		    	// 	output.reservations.push({
			    // 		"iSBN": result[0].ISBN
			    // 	});
		    	// }
		    	for(var i = 0; i < result.length; i++) {
		    		if(result[i].VALID == 1 && new Date(result[i].ENDDATE).withoutTime() >= new Date().withoutTime()) {
		    			output.reservations.push({
				    		"iSBN": result[0].ISBN,
				    		"bookName": result[i].BOOKNAME,
							"author": result[i].AUTHOR,
							"icon": result[i].ICON
				    	});
		    		}
		    	}
		    	res.json(output);
		    });
			return;
		} else {
			res.json([]);
			return;
		}
	});
});
// broken, should replaced immediately
appApi.get('/getborrowstatus', function(req, res) {
	var bookId = req.header("bookid");
	var sql = 'SELECT B.BOOKID, B.NOTBORROWS, R.ENDDATE, T.USERID, T2.CENTERID, T3.BORROWEDUSERID FROM BOOK B JOIN MEMBER M JOIN ( SELECT R.USERID FROM RESERVATION R ORDER BY R.ENDDATE DESC LIMIT 1 ) AS T LEFT JOIN RESERVATION R ON B.BOOKID=R.BOOKID AND R.USERID=M.USERID LEFT JOIN ( SELECT CB.CENTERID, B2.BOOKID FROM CENTERBRANCH CB, BOOK B2 WHERE CB.CENTERBRANCHID=B2.CENTERBRANCHID ) AS T2 ON T2.BOOKID=B.BOOKID LEFT JOIN ( SELECT BB.BOOKID, BB.USERID AS BORROWEDUSERID FROM BORROWSBOOK BB WHERE BB.RETURNDATE IS NULL ) AS T3 ON T3.BOOKID=B.BOOKID WHERE B.BOOKID=? ORDER BY R.ENDDATE DESC LIMIT 1';
	con.query(sql, [bookId], function(err, result) {
		if(err) throw err;
		if(result[0] != null) {
			res.json(
				{
					"bookId": result[0].BOOKID,
		            "canBeBorrowed": result[0].NOTBORROWS == 0 ? false : true,
		            "reservedEndDate": result[0].ENDDATE,
		            "reservedUserId": result[0].USERID,
		            "borrowingUserId": result[0].BORROWEDUSERID,
		            "centerId": result[0].CENTERID,
		            "DEVNOTE": "This is a broken api, should be replaced with api/getbookbyid immediately"
			    }
			);
			return;
		} else {
			res.json([]);
			return;
		}
	});
});
appApi.get('/borrow', function(req, res) {
	var bookIdsStr = req.header("bookids");
	var userId = req.header("userid");
	var token = req.header("token");
	var output = {'borrowedbooks': [], 'renewedbooks': []};
	var done = 0;
	var bookIds = bookIdsStr.split(" ");

	checkToken(token, function() {
		done -= bookIds.length;
		for(var i = 0; i < bookIds.length; i++) {
			if(isNormalInteger(bookIds[i])) {
				var sql = 'SELECT B.BOOKID, B.ISBN, B.NOTBORROWS, C.BORROWSTIMES AS ALLOWEDBORROWSTIMES, C.CENTERID AS BOOKCENTERID, T.RESERVEDUSERID, T.RESERVATIONENDDATE, T.RESERVATIONVALID, T2.BORROWSTIMES, T3.BORROWEDUSERID, T4.CENTERID AS MEMBERCENTERID, T4.VALIDSTATE, T4.VALIDDATE FROM BOOK B JOIN MEMBER M JOIN CENTER C JOIN CENTERBRANCH CB LEFT JOIN ( SELECT R2.USERID AS RESERVEDUSERID, B2.BOOKID, R2.ENDDATE AS RESERVATIONENDDATE, R2.VALID AS RESERVATIONVALID FROM RESERVATION R2, BOOK B2 WHERE B2.ISBN=R2.ISBN) AS T ON T.BOOKID=B.BOOKID LEFT JOIN ( SELECT BB.BOOKID, BB.BORROWSTIMES  FROM BORROWSBOOK BB  WHERE BB.RETURNDATE=0 AND BB.USERID=? ) AS T2 ON T2.BOOKID=B.BOOKID LEFT JOIN ( SELECT BB2.BOOKID, BB2.USERID AS BORROWEDUSERID  FROM BORROWSBOOK BB2  WHERE BB2.RETURNDATE IS NULL ) AS T3 ON T3.BOOKID=B.BOOKID JOIN ( SELECT M.CENTERID, M.VAILDTIME AS VALIDDATE, M.VAILDSTATE AS VALIDSTATE  FROM MEMBER M WHERE M.USERID=? ) AS T4 WHERE B.CENTERBRANCHID=CB.CENTERBRANCHID AND CB.CENTERID=C.CENTERID AND B.BOOKID=? LIMIT 1';
				con.query(sql, [userId, userId, bookIds[i]], function(err, result) {
					if(err) {
						throw err;
						done++;
						return;
					}
					if(result[0] != null) {
						// this book cannot be borrowed
						if(result[0].NOTBORROWS == 1) {
							done++;
							return;
						}
						// this book is reserved by others
						if(result[0].RESERVEDUSERID != userId && result[0].RESERVATIONVALID == 1 &&  new Date(result[0].RESERVATIONENDDATE).withoutTime() >= new Date().withoutTime()) {
							done++;
							return;
						}
						// this book is borrowed by others
						if(result[0].BORROWEDUSERID != userId && result[0].BORROWEDUSERID != null) {
							done++;
							return;
						}
						// this book is borrowed by him but up to renew limit
						if(result[0].BORROWSTIMES != null && result[0].BORROWSTIMES >= result[0].ALLOWEDBORROWSTIMES) {
							done++;
							return;
						}
						// centerid mismatch
						if(result[0].BOOKCENTERID != result[0].MEMBERCENTERID) {
							done++;
							return;
						}
						// restricted user
						if(result[0].VALIDSTATE == 0 || (new Date(result[0].VALIDDATE).withoutTime() < new Date().withoutTime() && result[0].VALIDDATE != null)) {
							done++;
							return;
						}
						// borrow process begins
						if(result[0].BORROWEDUSERID == null) {
							output.borrowedbooks.push(result[0].BOOKID);
							var sql = "INSERT INTO `borrowsbook` (`BorrowsID`, `BookID`, `UserID`, `BorrowsDate`, `LastRenewalDate`, `ReturnDate`, `BorrowsTimes`, `PayState`) VALUES (NULL, ?, ?, ?, ?, NULL, '0', '0')";
							con.query(sql, [result[0].BOOKID, userId, new Date(), new Date()]);
							sql = "UPDATE RESERVATION SET VALID=0 WHERE ISBN=? AND USERID=?";
							con.query(sql, [result[0].ISBN, userId]);
						} else {
							output.renewedbooks.push(result[0].BOOKID);
							renew(userId, result[0].BOOKID)
						}
						done++;
						return;
					} else {
						// the book just cannot be found
						done++;
						return;
					}
				});
			} else {
				done++;
			}
		}
		setInterval(function() {
			if(done == 0) {
				done++;
				res.json(output);
				return;
			}
		}, 300);
	}, function() {
		res.json({"error": "Unknown Token."});
	});
});
function renew(userId, bookId) {
	var sql = 'SELECT BB.BORROWSID, BB.BORROWSTIMES, BB.USERID, BB.RETURNDATE FROM BORROWSBOOK BB WHERE BB.BOOKID=? ORDER BY BB.RETURNDATE ASC LIMIT 1';
	con.query(sql, [bookId], function(err, result) {
		var sql = "UPDATE `borrowsbook` SET `BorrowsTimes` = ?, `LastRenewalDate` = ? WHERE `borrowsbook`.`BorrowsID` = ?";
		con.query(sql, [result[0].BORROWSTIMES + 1, new Date(), result[0].BORROWSID]);
	});
}
appApi.get('/return', function(req, res) {
	var bookIdsStr = req.header("bookids");
	var token = req.header("token");
	var output = {'returnedWithoutFine': [], 'returnedWithFine': [], 'fine': 0};
	var done = 0;
	var bookIds = bookIdsStr.split(" ");

	checkToken(token, function() {
		done -= bookIds.length;
		for(var i = 0; i < bookIds.length; i++) {
			if(isNormalInteger(bookIds[i])) {
				var sql = 'SELECT BB.BORROWSID, BB.BOOKID, BB.LASTRENEWALDATE, C.FINE, C.DATELIMIT FROM BORROWSBOOK BB JOIN CENTERBRANCH CB JOIN CENTER C JOIN BOOK B WHERE BB.RETURNDATE IS NULL AND C.CENTERID=CB.CENTERID AND CB.CENTERBRANCHID=B.CENTERBRANCHID AND B.BOOKID=BB.BOOKID AND BB.BOOKID=? LIMIT 1';
				con.query(sql, [bookIds[i]], function(err, result) {
					if(err) {
						throw err;
						done++;
						return;
					}
					// the book can be returned
					if(result.length == 1) {
						var overdueDays = (new Date(result[0].LASTRENEWALDATE).daysBetween(new Date())) + 1 - result[0].DATELIMIT;
						if(overdueDays > 0) {
							con.query("UPDATE BORROWSBOOK SET RETURNDATE=?, PAYSTATE=1 WHERE BORROWSID=?", [new Date(), result[0].BORROWSID]);
							output.fine += result[0].FINE * overdueDays;
							output.returnedWithFine.push(result[0].BOOKID);
						} else {
							con.query("UPDATE BORROWSBOOK SET RETURNDATE=?, PAYSTATE=0 WHERE BORROWSID=?", [new Date(), result[0].BORROWSID]);
							output.returnedWithoutFine.push(result[0].BOOKID);
						}
					}
					done++;
					return;
				});
			} else {
				done++;
				return;
			}
		}
		setInterval(function() {
			if(done == 0) {
				done++;
				res.json(output);
				return;
			}
		}, 300);
	}, function() {
		res.json({"error": "Unknown Token."});
	});
});

appApi.get('/showborrowhistorybyuserid', function(req, res) {
	var id = req.header("id");
	var output = {'unreturnedbooks': [], 'returnedbooks': []};

	var sql = 'SELECT * FROM BORROWSBOOK BB JOIN BOOK B JOIN CENTERBRANCH CB LEFT JOIN ( SELECT CB1.CENTERBRANCHID, C.DATELIMIT FROM CENTER C, CENTERBRANCH CB1 WHERE CB1.CENTERID=C.CENTERID ) AS T1 ON T1.CENTERBRANCHID = B.CENTERBRANCHID WHERE B.BOOKID=BB.BOOKID AND CB.CENTERBRANCHID=B.CENTERBRANCHID AND BB.USERID=?';
	con.query(sql, [id], function(err, result) {
		if(err) {
			throw err;
			return;
		}
		// finish the book can be returned
		for(var i = 0; i < result.length; i++) {
			if(result[i].ReturnDate == null) {
				var dueDate = result[i].LastRenewalDate.withoutTime().addDays(result[i].DATELIMIT - 1);
				output.unreturnedbooks.push(
					{
						"bookId": result[i].BookID,
						"bookName": result[i].BookName,
						"author": result[i].Author,
						"iSBN": result[i].ISBN,
						"publishDate": result[i].PublishDate,
						"icon": result[i].ICON,
						"borrowDate": result[i].BorrowsDate,
						"lastRenewalDate": result[i].LastRenewalDate,
						"renewalTime": result[i].BorrowsTime,
						"branchId": result[i].CenterBranchID,
						"branchName": result[i].BranchName,
						"dewey": result[i].Dewey,
						"dueDate": dueDate

					}
				);
			} else {
				output.returnedbooks.push(
					{
						"bookId": result[i].BookID,
						"bookName": result[i].BookName,
						"author": result[i].Author,
						"iSBN": result[i].ISBN,
						"publishDate": result[i].PublishDate,
						"icon": result[i].ICON,
						"borrowDate": result[i].BorrowsDate,
						"lastRenewalDate": result[i].LastRenewalDate,
						"renewalTime": result[i].BorrowsTime,
						"branchId": result[i].CenterBranchID,
						"branchName": result[i].BranchName,
						"dewey": result[i].Dewey,
						"returnDate": result[i].ReturnDate,
						"payState": result[i].PayState

					}
				);
			}
		}
		res.json(output);
		return;
	});
});

appApi.get('/listreservedbystaffid', function(req, res) {
	var id = req.header("id");
	var output = {'reserved': []};

	var sql = 'SELECT B.ISBN, B.BOOKNAME, B.AUTHOR, B.BOOKPOSITION, R.ENDDATE, R.RESERVATIONID, R.VALID, B.ICON FROM RESERVATION R, CENTERBRANCH CB, STAFF S, BOOK B WHERE CB.CENTERBRANCHID=S.CENTERBRANCHID AND B.CENTERBRANCHID=CB.CENTERBRANCHID AND R.ISBN=B.ISBN AND S.STAFFID=?';
	con.query(sql, [id], function(err, result) {
		if(err) {
			throw err;
			return;
		}
		for(var i = 0; i < result.length; i++) {
			if(result[i].VALID == 1 && new Date(result[i].ENDDATE).withoutTime() >= new Date().withoutTime()) {
				output.reserved.push({
					"iSBN": result[i].ISBN,
					"bookName": result[i].BOOKNAME,
					"author": result[i].AUTHOR,
					"bookPosition": result[i].POSITION,
					"endDate": result[i].ENDDATE,
					"reservationId": result[i].RESERVATIONID,
					"icon": result[i].ICON
				});
			}
		}
		res.json(output);
		return;
	});
});
appApi.get('/listreservedbyuserid', function(req, res) {
	var id = req.header("id");
	var output = {'reserved': []};
	// output.reserved.push({
	// 	"iSBN": "1234",
	// 	"bookName": "DEVBN",
	// 	"author": "DEVAU",
	// 	"bookPosition": "DEVBP",
	// 	"endDate": new Date(),
	// 	"reservationId": 123,
	// 	"icon": "DEVIC"
	// });

	var sql = 'SELECT B.ISBN, B.BOOKNAME, B.AUTHOR, B.BOOKPOSITION, R.ENDDATE, R.RESERVATIONID, R.VALID, B.ICON FROM RESERVATION R, BOOK B WHERE R.ISBN=B.ISBN AND R.USERID=?';
	con.query(sql, [id], function(err, result) {
		if(err) {
			throw err;
			return;
		}
		for(var i = 0; i < result.length; i++) {
			if(result[i].VALID == 1 && new Date(result[i].ENDDATE).withoutTime() >= new Date().withoutTime()) {
				output.reserved.push({
					"iSBN": result[i].ISBN,
					"bookName": result[i].BOOKNAME,
					"author": result[i].AUTHOR,
					"bookPosition": result[i].POSITION,
					"endDate": result[i].ENDDATE,
					"reservationId": result[i].RESERVATIONID,
					"icon": result[i].ICON
				});
			}
		}
		res.json(output);
		return;
	});
});
appApi.get('/searchbook', function(req, res) {
	var parameter = req.header("parameter");
	var parameterEscape = mysql.escape(parameter).split("'")[1];
	var userId = req.header("userId")
	var output = {'result': []};

	var sql = 'SELECT B.ISBN, B.BOOKID, B.BOOKNAME, B.AUTHOR, B.ICON FROM BOOK B JOIN MEMBER M JOIN CENTERBRANCH CB JOIN CENTER C WHERE M.CENTERID=C.CENTERID AND CB.CENTERID=C.CENTERID AND B.CENTERBRANCHID=CB.CENTERBRANCHID AND BOOKNAME LIKE "%' + parameter + '%" OR ISBN=? AND M.USERID=? GROUP BY ISBN';
	con.query(sql, [parameter, userId], function(err, result) {
		if(err) {
			throw err;
			return;
		}
		for(var i = 0; i < result.length; i++) {
			output.result.push({
				"iSBN": result[i].ISBN,
				"bookName": result[i].BOOKNAME,
				"author": result[i].AUTHOR,
				"icon": result[i].ICON
			});
		}
		res.json(output);
		return;
	});
});
appApi.get('/searchbookdetail', function(req, res) {
	var iSBN = req.header("isbn") + "";
	var userId = req.header("userid");
	var output = {'branches': []};

	var sql = 'SELECT CB.CENTERBRANCHID, CB.BRANCHNAME, B.ISBN, B.NOTRESERVATION, T1.COUNTRID, COUNT(B.BOOKID) AS COUNTBID, T2.COUNTBORROWBID FROM CENTERBRANCH CB JOIN MEMBER M JOIN CENTER C JOIN BOOK B LEFT JOIN ( SELECT COUNT(R.RESERVATIONID) AS COUNTRID, R.ISBN, R.USERID FROM RESERVATION R WHERE R.VALID=1 AND DATE(R.ENDDATE) >= DATE(NOW()) GROUP BY R.ISBN ) AS T1 ON T1.ISBN=B.ISBN AND T1.USERID=M.USERID LEFT JOIN ( SELECT COUNT(BB.BOOKID) AS COUNTBORROWBID, B2.ISBN, BB.USERID FROM BORROWSBOOK BB, BOOK B2 WHERE BB.BOOKID=B2.BOOKID AND BB.RETURNDATE=NULL ) AS T2 ON T2.ISBN=B.ISBN AND T2.USERID=M.USERID WHERE C.CENTERID=M.CENTERID AND CB.CENTERID=C.CENTERID AND B.CENTERBRANCHID=CB.CENTERBRANCHID AND M.USERID=? AND B.ISBN=?';
	con.query(sql, [userId, iSBN], function(err, result) {
		if(err) {
			throw err;
			return;
		}
		var countBook = 0;
		var countReserved = 0;
		var countBorrowed = 0;
		for(var i = 0; i < result.length; i++) {
			countBook = 0;
			countReserved = 0;
			countBorrowed = 0;
			if(result[i].COUNTBID != null) {
				countBook = result[i].COUNTBID;
			}
			if(result[i].COUNTRID != null) {
				countReserved = result[i].COUNTRID;
			}
			if(result[i].COUNTBORROWBID != null) {
				countBorrowed = result[i].COUNTBORROWBID;
			}
			output.branches.push({
				"branchId": result[i].CENTERBRANCHID,
				"branchName": result[i].BRANCHNAME,
				"bookLeft": countBook - countReserved - countBorrowed
			});
		}
		res.json(output);
		return;
	});
});
appApi.get('/topbooks', function(req, res) {
	var userId = req.header("userid");
	var output = {'books': []};
	// output.books.push({
	// 	"iSBN": "iSBN",
	// 	"bookName": "bookName",
	// 	"author": "author",
	// 	"icon": "icon",
	// 	"urlImage": "urlImage"
	// });
	// output.books.push({
	// 	"iSBN": "iSBN",
	// 	"bookName": "bookName",
	// 	"author": "author",
	// 	"icon": "icon",
	// 	"urlImage": "urlImage"
	// });
	// output.books.push({
	// 	"iSBN": "iSBN",
	// 	"bookName": "bookName",
	// 	"author": "author",
	// 	"icon": "icon",
	// 	"urlImage": "urlImage"
	// });
	// output.books.push({
	// 	"iSBN": "iSBN",
	// 	"bookName": "bookName",
	// 	"author": "author",
	// 	"icon": "icon",
	// 	"urlImage": "urlImage"
	// });

	var sql = 'SELECT B.ISBN, B.ICON, B.BOOKNAME, B.AUTHOR, B.URLIMAGE, BB.BORROWSDATE, COUNT(BB.BORROWSID) AS COUNTBBID FROM BOOK B JOIN CENTER C JOIN MEMBER M JOIN CENTERBRANCH CB JOIN BORROWSBOOK BB WHERE C.CENTERID=M.CENTERID AND CB.CENTERID=C.CENTERID AND B.CENTERBRANCHID=CB.CENTERBRANCHID AND BB.BOOKID=B.BOOKID AND BB.BORROWSDATE > NOW() - INTERVAL 30 DAY AND M.USERID=? GROUP BY B.ISBN ORDER BY COUNTBBID DESC LIMIT 10';
	con.query(sql, [userId], function(err, result) {
		if(err) {
			throw err;
			return;
		}
		for(var i = 0; i < result.length; i++) {
			output.books.push({
				"iSBN": result[i].ISBN,
				"bookName": result[i].BOOKNAME,
				"author": result[i].AUTHOR,
				"icon": result[i].ICON,
				"urlImage": result[i].URLIMAGE
			});
		}
		res.json(output);
		return;
	});
});
appApi.get('/newbooks', function(req, res) {
	var userId = req.header("userid");
	var output = {'books': []};

	var sql = 'SELECT DISTINCT B.ISBN, B.ICON, B.BOOKNAME, B.AUTHOR, B.URLIMAGE, B.ENTRYDATE FROM BOOK B JOIN CENTER C JOIN MEMBER M JOIN CENTERBRANCH CB JOIN BORROWSBOOK BB WHERE C.CENTERID=M.CENTERID AND CB.CENTERID=C.CENTERID AND B.CENTERBRANCHID=CB.CENTERBRANCHID AND BB.BOOKID=B.BOOKID AND M.USERID=? ORDER BY B.ENTRYDATE DESC LIMIT 10';
	con.query(sql, [userId], function(err, result) {
		if(err) {
			throw err;
			return;
		}
		for(var i = 0; i < result.length; i++) {
			output.books.push({
				"iSBN": result[i].ISBN,
				"bookName": result[i].BOOKNAME,
				"author": result[i].AUTHOR,
				"icon": result[i].ICON,
				"urlImage": result[i].URLIMAGE
			});
		}
		res.json(output);
		return;
	});
});
appApi.get('/verifylogin', function(req, res) {
	var token = req.header("token");
	checkToken(token, function() {
		res.json({"result": "Login Sucessful"});
	}, function() {
		res.json({});
	});
});
appApi.get('/changepassword', function(req, res) {
	var id = req.header("id");
	var type = req.header("type");
	var oldPassword = req.header("oldpassword");
	var oldPasswordHash = oldPassword.hash();
	var newPassword = req.header("newpassword");
	var newPasswordHash = newPassword.hash();
	var sql = "";
	if(type == "M") {
		sql = 'UPDATE MEMBER SET PASSWORD=? WHERE USERID=? AND PASSWORD=?';
		
	} else {
		sql = 'UPDATE STAFF SET PASSWORD=? WHERE STAFFID=? AND PASSWORD=?';
	}
	con.query(sql, [newPasswordHash, id, oldPasswordHash], function(err, result){
		if(err) {
			throw err;
			return;
		}
		if(result.affectedRows > 0) {
			res.json({"result": "Change Password Sucessful"});
		} else {
			res.json({});
		}
	});
});
appApi.get('/setischinese', function(req, res) {
	var id = req.header("id");
	var type = req.header("type");
	var isChinese = req.header("ischinese");
	var token = req.header("token");
	checkToken(token, function() {
		var sql = "";
		if(type == "M") {
			sql = 'UPDATE MEMBER SET ISCHINESE=? WHERE USERID=?';
			
		} else {
			sql = 'UPDATE STAFF SET ISCHINESE=? WHERE STAFFID=?';
		}
		con.query(sql, [isChinese, id], function(err, result){
			if(err) {
				throw err;
				return;
			}
			if(result.affectedRows > 0) {
				res.json({"result": "Change IsChinese Sucessful"});
			} else {
				res.json({});
			}
		});
	}, function() {
		res.json({});
	});
});
appApi.get('/listnoicon', function(req, res) {
	var staffId = req.header("staffid");
	var token = req.header("token");
	var output = {"books": []};
	var sql = 'SELECT DISTINCT B.ISBN, B.BOOKNAME, B.AUTHOR, B.URLIMAGE FROM BOOK B JOIN STAFF S JOIN CENTERBRANCH CB WHERE S.CENTERBRANCHID=CB.CENTERBRANCHID AND CB.CENTERBRANCHID=B.CENTERBRANCHID AND B.ICON IS NULL AND S.STAFFID=?';
	con.query(sql, [staffId], function(err, result){
		if(err) {
			throw err;
			return;
		}
		for(var i = 0; i < result.length; i++) {
			output.books.push({
				"iSBN": result[i].ISBN,
				"bookName": result[i].BOOKNAME,
				"author": result[i].AUTHOR,
				"urlImage": result[i].URLIMAGE
			});
		}
		res.json(output);
	});
});
appApi.post('/updatenoicon', function(req, res) {
	res.json({"error": "discontinued"});
	return;
	req.setEncoding('utf8');
	var token = req.header("token");
	var iSBN = req.header("isbn");
	var extension = req.header("extension");
	// var base64String = req.body;
	var base64String = "";
	var buf = '';
	req.on('data', function(chunk){ buf += chunk });
	req.on('end', function() {
		base64String = buf;
		var first = buf.trim()[0];
		checkToken(token, function(){

			var sql = 'UPDATE BOOK B SET B.ICON=? WHERE B.ISBN=?';
			con.query(sql, [extension == "" ? null : extension, iSBN], function(err, result){
				if(err) {
					throw err;
					return;
				}
				if(result.affectedRows > 0) {
					res.json({"result": "SUCCESS", "DEV": base64String});
				} else {
					res.json({});
				}
			});
		}, function() {
			res.json({});
		});
	});
	
});
// appApi.get('logout', function(req, res) {
// 	var sql = '';
// 	con.query(sql, [], function(err, result) {
		
// 	});
// });
appApi.get('/searchhint', function(req, res) {
	var parameter = req.header("parameter");
	var output = {'result': []};

	var sql = 'SELECT BOOKNAME FROM BOOK WHERE BOOKNAME LIKE "%?%"';
	con.query(sql, [parameter], function(err, result) {
		if(err) {
			throw err;
			return;
		}
		for(var i = 0; i < result.length; i++) {
			output.reserved.push({
				"bookId": result[i].BOOKID,
				"bookName": result[i].BOOKNAME,
				"author": result[i].AUTHOR,
				"icon": result[i].ICON
			});
		}
		res.json(output);
		return;
	});
});
appApi.get('/addreservation', function(req, res) {
	// LATER DEV: add checking of duplicated reservation
	var userId = req.header("userid");
	var token = req.header("token");
	var iSBN = req.header("isbn");

	checkToken(token, function() {
		var sql = "SELECT C.RESERVATIONDATELIMIT, M.VAILDSTATE AS VALIDSTATE, M.VAILDTIME AS VALIDDATE FROM RESERVATION R JOIN MEMBER M JOIN CENTER C WHERE M.CENTERID=C.CENTERID AND R.USERID=M.USERID AND M.USERID=? GROUP BY M.USERID";
		con.query(sql, [userId], function(err, result) {
			if(err) {
				throw err;
				return;
			}
			var sql = "INSERT INTO RESERVATION (`ReservationID`, `BookID`, `ISBN`, `UserID`, `ReservationDate`, `EndDate`, `Valid`) VALUES (NULL, '', ?, ?, ?, ?, '1')";
			con.query(sql, [iSBN, userId, new Date(), new Date().withoutTime().addDays(result[0].RESERVATIONDATELIMIT - 1)], function(err, result) {
				if(err) {
					throw err;
					return;
				}
				if(result.affectedRows > 0) {
					res.json({"result": "Add Reservation Successful"});
				} else {
					res.json({});
				}
			});
			// res.json({
			// 	"userId": userId,
			// 	"iSBN": iSBN,
			// 	"new Date()": new Date(),
			// 	"RESERVATIONDATELIMIT": result.RESERVATIONDATELIMIT
			// });
		});
	}, function() {
		res.json({"error": "Unknown Token."});
	});
});
appApi.get('/removereservation', function(req, res) {
	var userId = req.header("userid");
	var token = req.header("token");
	var iSBN = req.header("isbn");
	
	checkToken(token, function() {
		var sql = "UPDATE RESERVATION R SET R.VALID=0 WHERE R.VALID=1 AND R.USERID=? AND R.ISBN=? ORDER BY R.ENDDATE ASC LIMIT 1";
		con.query(sql, [userId, iSBN], function(err, result){
			if(err) {
				throw err;
				return;
			}
			if(result.affectedRows > 0) {
				res.json({"result": "Delete Reservation Sucessful"});
			} else {
				res.json({});
			}
		});
	}, function() {
		res.json({"error": "Unknown Token."});
	});
});
Date.prototype.withoutTime = function () {
    var d = new Date(this);
    d.setHours(0, 0, 0, 0);
    return d;
}
Date.prototype.daysBetween = function(date2) {
  //Get 1 day in milliseconds
  var one_day=1000*60*60*24;

  // Convert both dates to milliseconds
  var date1_ms = this.withoutTime().getTime();
  var date2_ms = date2.withoutTime().getTime();

  // Calculate the difference in milliseconds
  var difference_ms = date2_ms - date1_ms;
    
  // Convert back to days and return
  return difference_ms / one_day;
}
Date.prototype.addDays = function(days) {
  var dat = new Date(this.valueOf());
  dat.setDate(dat.getDate() + days);
  return dat;
}
String.prototype.shuffle = function () {
    var a = this.split(""),
        n = a.length;

    for(var i = n - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }
    return a.join("");
}
String.prototype.hash = function() {
	return crypto.createHash('sha512').update(this.slice(0)).digest('hex').toUpperCase();
}
function isNormalInteger(str) {
    return /^\+?([1-9]\d*)$/.test(str);
}
function checkToken(token, callback, callbackFail) {
	if(token == "TOKENFORTESTING") {
		callback();
		return;
	}
	var tokenHash = token.hash();
	con.query("SELECT * FROM MEMBER WHERE TOKENHASH=?", [tokenHash], function(err, result) {
		if(result[0] != null) {
			callback();
		} else {
			con.query("SELECT * FROM STAFF WHERE TOKENHASH=?", [tokenHash], function(err, result) {
				if(result[0] != null) {
					callback();
				} else {
					callbackFail();
				}
			});
		}
	});
}