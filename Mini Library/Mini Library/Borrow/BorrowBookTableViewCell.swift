//
//  BorrowBookTableViewCell.swift
//  Mini Library
//
//  Created by FearLix Yeung on 20/12/2017.
//  Copyright © 2017 lf17ml. All rights reserved.
//

import UIKit

class BorrowBookTableViewCell: UITableViewCell {
    @IBOutlet weak var titleUILabel: UILabel!
    @IBOutlet weak var subTitleUILabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
