//
//  BorrowViewController.swift
//  Mini Library
//
//  Created by FearLix Yeung on 19/12/2017.
//  Copyright © 2017 lf17ml. All rights reserved.
//

import UIKit
import AVFoundation
import TTGSnackbar

class BorrowViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var previewUIView: UIView!
    @IBOutlet weak var borrowBookUITableView: UITableView!
    @IBOutlet weak var borrowerUILabel: UILabel!
    // This constraint ties an element at zero points from the bottom layout guide
//    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    
    var borrowingBookIds = [Int]()
    var borrowingBookNames = [String]()
    var borrowingBookStatus = [String]()
    var reservedBookISBNs = [String]()
    var reservedBookNames = [String]()
    var userId = 0
    var userCenterId = 0
    let token = UserDefaults.standard.object(forKey: "token") as! String
    let captureSession = AVCaptureSession()
    private let supportedCodeTypes = [
        AVMetadataObject.ObjectType.code128 // its the one
//        AVMetadataObject.ObjectType.qr
    ]
    var previewLayer: AVCaptureVideoPreviewLayer?
    var captureDevice: AVCaptureDevice!
    var qrCodeFrameView: UIView?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        prepareCamera()
        self.hideKeyboard()
    }
    
    
    
    @IBAction func borrowUIButton(_ sender: Any) {
        var borrowStr = ""
        for book in borrowingBookIds {
            borrowStr.append(String(book) + " ")
        }
        let url = URL.init(string: "http://13.230.65.43:3000/api/borrow")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue(borrowStr, forHTTPHeaderField: "bookids")
        request.addValue(String(userId), forHTTPHeaderField: "userid")
        request.addValue(token, forHTTPHeaderField:  "token")
        performRequest(request: request, completion: {(data, code, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            if let successDic = jsonUnwrap(targetObject: data) as? [String: Any] {
                print((successDic["borrowedbooks"] as! [Int]).count)
                print((successDic["renewedbooks"] as! [Int]).count)
                print(self.borrowingBookIds.count)
                if (successDic["borrowedbooks"] as! [Int]).count + (successDic["renewedbooks"] as! [Int]).count == self.borrowingBookIds.count {
                    DispatchQueue.main.async {
                        TTGSnackbar(message: NSLocalizedString("SUCCESS: All Book Borrowed", comment: ""), duration: .middle).show()
                    }
                    self.borrowingBookIds.removeAll()
                    self.borrowingBookNames.removeAll()
                    self.borrowingBookStatus.removeAll()
                    DispatchQueue.main.async {self.borrowBookUITableView.reloadData()}
                } else {
                    var successBooks = ""
                    var renewBooks = ""
                    var allSuccesses = [Int]()
                    if successDic["borrowedbooks"] != nil {
                        successBooks = (successDic["borrowedbooks"] as! [Int]).map(String.init).joined(separator: ", ")
                        for successBook in successDic["borrowedbooks"] as! [Int] {
                            allSuccesses.append(successBook)
                        }
                    }
                    if successDic["renewedbooks"] != nil {
                        renewBooks += (successDic["renewedbooks"] as! [Int]).map(String.init).joined(separator: ", ")
                        for renewBook in successDic["renewedbooks"] as! [Int] {
                            allSuccesses.append(renewBook)
                        }
                    }
                    for allSuccess in allSuccesses {
                        if self.borrowingBookIds.contains(allSuccess) {
                            for (i, match) in self.borrowingBookIds.enumerated() {
                                if match == allSuccess {
                                    self.borrowingBookIds.remove(at: i)
                                    self.borrowingBookNames.remove(at: i)
                                    self.borrowingBookStatus.remove(at: i)
                                }
                            }
                        }
                    }
                    DispatchQueue.main.async {self.borrowBookUITableView.reloadData()}
                    let alert = UIAlertController(title: NSLocalizedString("FAIL: Not All Book Borrowed", comment: ""), message: NSLocalizedString("Borrowed Books: \(successBooks)\nRenewed Books: \(renewBooks)", comment: "FAIL: Not All Book Borrowed DETAIL"), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .`default`, handler: { _ in
                        NSLog("The \"OK\" alert occured.")
                    }))
                    self.present(alert, animated: true, completion: nil)

                }
            }
        })
    }
    
    @IBAction func addBookIdDEOE(_ sender: Any) {
        let senderUITextField = sender as! UITextField
        if let input = Int(senderUITextField.text!) {
            addBook(id: input)
            senderUITextField.text = ""
        }
    }
    @IBAction func borrowerIdDEOE(_ sender: Any) {
        let senderUITextField = sender as! UITextField
        if let input = Int(senderUITextField.text!) {
            changeMember(id: input)
            senderUITextField.text = ""
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func prepareCamera() {
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        let availableDevices = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .back).devices
        captureDevice = availableDevices.first
        if captureDevice != nil {
            beginSession()
        }
    }
    func beginSession() {
        do {
//            check permission from user
            let captureDeviceInput = try AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(captureDeviceInput)
        } catch {
            print(error.localizedDescription)
        }
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        let bounds = previewUIView.bounds
        previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        previewLayer?.bounds = bounds
        previewUIView.layer.addSublayer(previewLayer!)
        previewLayer?.frame = previewUIView.layer.frame
        captureSession.startRunning()
        
//        frame highlighting
        qrCodeFrameView = UIView()
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            previewUIView.addSubview(qrCodeFrameView)
            previewUIView.bringSubview(toFront: qrCodeFrameView)
        }
        
        
        let metaDataOutput = AVCaptureMetadataOutput()
//        if captureSession.canAddOutput(metaDataOutput) {
            captureSession.addOutput(metaDataOutput)
//        }
        metaDataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        metaDataOutput.metadataObjectTypes = supportedCodeTypes
        captureSession.commitConfiguration()
    }
    override func viewDidLayoutSubviews() {
        previewLayer?.position = CGPoint(x: previewUIView.frame.width/2, y: previewUIView.frame.height/2)
    }
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
//        print("metadataOutput")
        if metadataObjects.count == 0 {
//            qrCodeFrameView?.frame = CGRect.zero
            return
        }
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
//        if metadataObj.type == AVMetadataObject.ObjectType.qr
        
        if metadataObj.stringValue != nil {
//            update qrframe
            let barCodeObject = previewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            let reading = metadataObj.stringValue
            if let subReading = reading?.split(separator: "-", maxSplits: 2) {
                if subReading.count == 2 {
                    if subReading[0] == "B" {
                        if !borrowingBookIds.contains(Int(subReading[1])!) {
                            addBook(id: Int(subReading[1])!)
                        }
                    } else if subReading[0] == "M" {
                        if userId != Int(subReading[1])! {
                            changeMember(id: Int(subReading[1])!)
                        }
                    }
                }
            }
        }
    }
    func loadAllBooks() {
        for (i, book) in borrowingBookIds.enumerated() {
            loadBook(id: book, index: i, userId: self.userId)
        }
    }
    func addBook(id: Int) {
        borrowingBookIds.append(id)
        borrowingBookNames.append("")
        borrowingBookStatus.append("")
        borrowBookUITableView.beginUpdates()
        borrowBookUITableView.insertRows(at: [IndexPath(row: borrowingBookIds.count - 1, section: 1)], with: .automatic)
        borrowBookUITableView.endUpdates()
        loadBook(id: id, index: borrowingBookIds.count - 1, userId: userId)
    }
    func loadBook(id: Int, index: Int, userId: Int) {
        let url = URL.init(string: "http://13.230.65.43:3000/api/getbookbyid")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue(String(id), forHTTPHeaderField: "id")
        performRequest(request: request, completion: {(data, code, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            if let bookDic = jsonUnwrap(targetObject: data) as? [String: Any] {
                self.borrowingBookNames[index] = bookDic["name"] as! String
                if (bookDic["noBorrow"] as! Int) == 1 {
                    self.borrowingBookStatus[index] = NSLocalizedString("FAIL: Book No Borrow", comment: "")
                    self.borrowBookUITableView.reloadData()
                } else if userId != 0 {
                    //DEV check center
                    if bookDic["reservationEndDate"] as? String != nil && (bookDic["reservationEndDate"] as! String).jSStringToDate() > Date.init() {
                        if id == bookDic["reservedUserId"] as! Int {
                            self.borrowingBookStatus[index] = NSLocalizedString("SUCCESS: Reserver Borrowing", comment: "")
                        } else {
                            self.borrowingBookStatus[index] = NSLocalizedString("FAIL: Book Reserved By Others", comment: "")
                        }
                    } else if bookDic["borrowedUserId"] as? String != nil && (bookDic["borrowedUserId"] as! Int) != userId {
                        self.borrowingBookStatus[index] = NSLocalizedString("FAIL: UserId And BorrowingUserId Mismatch", comment: "")
                    } else {
                        self.borrowingBookStatus[index] = NSLocalizedString("SUCCESS: Book Can Be Borrowed", comment: "")
                    }
                }
            } else {
                self.borrowingBookNames[index] = NSLocalizedString("ERROR: No Such Book ID", comment: "")
            }
            DispatchQueue.main.async {self.borrowBookUITableView.reloadData()}
        })
    }
    func changeMember(id: Int) {
        userId = id
        borrowerUILabel.text = String(id)
        loadMember(id: id)
    }
    func loadMember(id: Int) {
        let url = URL.init(string: "http://13.230.65.43:3000/api/getmemberbyid")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue(String(id), forHTTPHeaderField: "id")
        performRequest(request: request, completion: {(data, code, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            if let memberDic = jsonUnwrap(targetObject: data) as? [String: Any] {
                DispatchQueue.main.async {self.borrowerUILabel.text = String(id) + " " + (memberDic["firstName"] as! String) + " " + (memberDic["lastName"] as! String)}
//                check validity
                if memberDic["validTime"] as? String != nil && (memberDic["validTime"] as! String).jSStringToDate() < Date.init() {
//                valid time over
                    DispatchQueue.main.async {self.borrowerUILabel.text = String(id) + " " + (memberDic["firstName"] as! String) + " " + (memberDic["lastName"] as! String) + " " + NSLocalizedString("ERROR: valid time over", comment: "")}
                } else if memberDic["validState"] as! Int != 1 {
//                valid code invalid
                    DispatchQueue.main.async {self.borrowerUILabel.text = String(id) + " " + (memberDic["firstName"] as! String) + " " + (memberDic["lastName"] as! String) + " " + NSLocalizedString("ERROR: valid code invalid", comment: "")}
                } else {
                    self.userCenterId = memberDic["centerId"] as! Int
                    self.reservedBookISBNs.removeAll(keepingCapacity: false)
                    self.reservedBookNames.removeAll(keepingCapacity: false)
                    for reservation in memberDic["reservations"] as! [[String: Any]] {
                        self.reservedBookISBNs.append(reservation["iSBN"] as! String)
                        self.reservedBookNames.append(reservation["bookName"] as! String)
                    }
                    DispatchQueue.main.async {self.borrowBookUITableView.reloadData()}
                    self.loadAllBooks()
                }
            } else {
                DispatchQueue.main.async {self.borrowerUILabel.text = String(id) + " " + NSLocalizedString("ERROR: No Such User ID", comment: "")}
            }
        })
    }
//    table
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return reservedBookISBNs.count
        case 1:
            return borrowingBookIds.count + 1
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "borrowBookTableViewCell", for: indexPath) as? BorrowBookTableViewCell else {
                fatalError("The dequeued cell is not an instance of BorrowBookTableViewCell.")
            }
            cell.titleUILabel.text = reservedBookNames[indexPath.row]
            cell.subTitleUILabel.text = reservedBookISBNs[indexPath.row]
            return cell
        case 1:
            if indexPath.row == borrowingBookIds.count {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "addTableViewCell", for: indexPath) as? AddTableViewCell else {
                    fatalError("The dequeued cell is not an instance of AddTableViewCell.")
                }
                return cell
            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "borrowBookTableViewCell", for: indexPath) as? BorrowBookTableViewCell else {
                    fatalError("The dequeued cell is not an instance of BorrowBookTableViewCell.")
                }
                cell.titleUILabel.text = borrowingBookNames[indexPath.row]
                cell.subTitleUILabel.text = String(borrowingBookIds[indexPath.row]) + " " + borrowingBookStatus[indexPath.row]
                return cell
            }
        default:
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return NSLocalizedString("Reserved Books", comment: "")
        case 1:
            return NSLocalizedString("Borrowing Books", comment: "")
        default:
            return nil
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            return
        case 1:
            if indexPath.row == borrowingBookIds.count {
                
            }
            return
        default:
            return
        }
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "borrowingSegue" {
            let controller = segue.destination as! BookDetailTableViewController
            if let indexPath = self.borrowBookUITableView.indexPathForSelectedRow {
                if indexPath.section == 1 {
                    controller.bookId = borrowingBookIds[indexPath.row]
                } else {
                    print("ERROR: prepare segue indexPath.section not equals 1")
                }
            }
        }
        
    }

}
