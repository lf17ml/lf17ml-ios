//
//  TabViewController.swift
//  Mini Library
//
//  Created by FearLix Yeung on 3/12/2017.
//  Copyright © 2017 lf17ml. All rights reserved.
//

import UIKit

class TabViewController: UIViewController {
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet weak var containerUIView: UIView!
    @IBOutlet weak var userPhotoUIImageView: UIImageView!
    var viewControllers: [UIViewController]!
    var selectedIndex: Int = 0
    let myStoryboard = UIStoryboard(name: "Main", bundle: nil)
    var userId: Int = 0
    var searchBarInputTemp = ""
    let inactiveIcons = [
        UIImage(named: "home"),
        UIImage(named: "barcode"),
        UIImage(named: "history"),
        UIImage(named: "profile")
    ]
    let activeIcons = [
        UIImage(named: "homeActive"),
        UIImage(named: "barcodeActive"),
        UIImage(named: "historyActive"),
        UIImage(named: "profileActive")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()

        // Do any additional setup after loading the view.
        //draw userPhoto
        userPhotoUIImageView.layer.borderWidth=1.0
        userPhotoUIImageView.layer.masksToBounds = false
        userPhotoUIImageView.layer.borderColor = UIColor.red.cgColor
        userPhotoUIImageView.layer.cornerRadius = userPhotoUIImageView.frame.size.height/2
        userPhotoUIImageView.clipsToBounds = true
        
        let v1 = myStoryboard.instantiateViewController(withIdentifier: "searchViewController") as! SearchViewController
        v1.userId = self.userId
        let v2 = myStoryboard.instantiateViewController(withIdentifier: "barcodeViewController") as! BarcodeViewController
        v2.userId = self.userId
        let v3 = myStoryboard.instantiateViewController(withIdentifier: "historyViewController") as! HistoryViewController
        v3.userId = self.userId
        let v4 = myStoryboard.instantiateViewController(withIdentifier: "settingMemberTableViewController") as! SettingMemberTableViewController
        v4.userId = self.userId
        v4.type = "M"
        viewControllers = [v1, v2, v3, v4]
        
        didPressTab(buttons[0])
        
        //        load what you have to load :)
//        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroud1")!)
        for (i, button) in buttons.enumerated() {
//            print("Item \(index): \(element)")
            button.contentMode = .center
            button.imageView?.contentMode = .scaleAspectFit
            button.setImage(inactiveIcons[i], for: UIControlState.normal)
            button.setImage(activeIcons[i], for: UIControlState.highlighted)
            button.setImage(activeIcons[i], for: UIControlState.selected)
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        (viewControllers[0] as! SearchViewController).searchBarInputTemp = self.searchBarInputTemp
        searchBarInputTemp = ""
    }
    @IBAction func didPressTab(_ sender: UIButton) {
        let previousIndex = selectedIndex
        selectedIndex = sender.tag
        buttons[previousIndex].isSelected = false
        let previousVC = viewControllers[previousIndex]
        previousVC.willMove(toParentViewController: nil)
        previousVC.view.removeFromSuperview()
        previousVC.removeFromParentViewController()
        sender.isSelected = true
        let vc = viewControllers[selectedIndex]
        addChildViewController(vc)
        vc.view.frame = containerUIView.bounds
        containerUIView.addSubview(vc.view)
        containerUIView.bringSubview(toFront: vc.view)
        vc.didMove(toParentViewController: self)
        buttons[selectedIndex].isSelected = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillAppear(animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillDisappear(animated)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
