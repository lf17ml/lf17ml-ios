//
//  SettingPasswordViewController.swift
//  Mini Library
//
//  Created by FearLix Yeung on 26/3/2018.
//  Copyright © 2018 lf17ml. All rights reserved.
//

import UIKit

class SettingPasswordViewController: UIViewController {
    @IBOutlet weak var oldPasswordUITextField: UITextField!
    @IBOutlet weak var passwordUITextField: UITextField!
    @IBOutlet weak var confirmPasswordUITextField: UITextField!
    var id:Int = 0
    var type = ""
    @IBAction func confirmTUI(_ sender: Any) {
        if passwordUITextField.text != confirmPasswordUITextField.text {
            let alert = UIAlertController(title: NSLocalizedString("Password mismatch", comment: ""), message: NSLocalizedString("Please try again.", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            DispatchQueue.main.async {self.present(alert, animated: true)}
            passwordUITextField.text = ""
            confirmPasswordUITextField.text = ""
            return
        }
        let url = URL.init(string: "http://13.230.65.43:3000/api/changepassword")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue(String(id), forHTTPHeaderField: "id")
        request.addValue(type, forHTTPHeaderField: "type")
        request.addValue(oldPasswordUITextField.text!, forHTTPHeaderField: "oldpassword")
        request.addValue(passwordUITextField.text!, forHTTPHeaderField: "newpassword")
        performRequest(request: request, completion: {(data, code, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            if let successDic = jsonUnwrap(targetObject: data) as? [String: Any] {
                if successDic.isEmpty {
                    let alert = UIAlertController(title: NSLocalizedString("Fail to change password", comment: ""), message: NSLocalizedString("Please try again.", comment: ""), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                    DispatchQueue.main.async {self.present(alert, animated: true)}
                    return
                }
                //                Login successful
                let alert = UIAlertController(title: NSLocalizedString("Change password success", comment: ""), message: NSLocalizedString("Going back.", comment: ""), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {action in
                    self.navigationController?.popViewController(animated: true)
                }))
                DispatchQueue.main.async {self.present(alert, animated: true)}
            } else {
                //                fail DEV
            }
        })
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
