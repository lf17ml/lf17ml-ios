//
//  Controller.swift
//  Mini Library
//
//  Created by FearLix Yeung on 14/12/2017.
//  Copyright © 2017 lf17ml. All rights reserved.
//

import Foundation
import CoreImage
import UIKit
import TTGSnackbar

func performRequest(request: URLRequest, completion: @escaping (_ data: Data?, _ hTTPStatusCode: Int, _ error: Error?) -> Void) {
//    var request = URLRequest(url: targetURL)
//    request.httpMethod = "GET"
    let sessionConfiguration = URLSessionConfiguration.default
    let session = URLSession(configuration: sessionConfiguration)
    let task = session.dataTask(with: request) { (data, response, error) in
        if let data = data {
            if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode {
                completion(data, response.statusCode, error)
            }
        }
    }
    task.resume()
}
func jsonUnwrap(targetObject: Data!) -> Any {
    do {
        return try JSONSerialization.jsonObject(with: targetObject, options: [])
    } catch let error as NSError {
        print(error)
    }
    return []
}
extension String
{
    func jSStringToDate() -> Date
    {
        //Create Date Formatter
        let dateFormatter = DateFormatter()
        
        //Specify Format of String to Parse
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        //Parse into NSDate
        let dateFromString : Date = dateFormatter.date(from: self)!
        
        //Return Parsed Date
        return dateFromString
    }
    func toDate() -> Date
    {
        //Create Date Formatter
        let dateFormatter = DateFormatter()
        
        //Specify Format of String to Parse
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        //Parse into NSDate
        let dateFromString : Date = dateFormatter.date(from: self)!
        
        //Return Parsed Date
        return dateFromString
    }
//    func mysqlDateToReadableString() -> String {
//        let input = self.jSStringToDate()
//
//    }
}
func generateBarcode(from string: String) -> UIImage? {
    
    let data = string.data(using: String.Encoding.ascii)
    
    if let filter = CIFilter(name: "CICode128BarcodeGenerator") {
        filter.setDefaults()
        //Margin
        filter.setValue(7.00, forKey: "inputQuietSpace")
        filter.setValue(data, forKey: "inputMessage")
        //Scaling
        let transform = CGAffineTransform(scaleX: 3, y: 3)
        
        if let output = filter.outputImage?.transformed(by: transform) {
            let context:CIContext = CIContext.init(options: nil)
            let cgImage:CGImage = context.createCGImage(output, from: output.extent)!
            let rawImage:UIImage = UIImage.init(cgImage: cgImage)
            
            //Refinement code to allow conversion to NSData or share UIImage. Code here:
            //http://stackoverflow.com/questions/2240395/uiimage-created-from-cgimageref-fails-with-uiimagepngrepresentation
            let cgimage: CGImage = (rawImage.cgImage)!
            let cropZone = CGRect(x: 0, y: 0, width: Int(rawImage.size.width), height: Int(rawImage.size.height))
            let cWidth: size_t  = size_t(cropZone.size.width)
            let cHeight: size_t  = size_t(cropZone.size.height)
            let bitsPerComponent: size_t = cgimage.bitsPerComponent
            //THE OPERATIONS ORDER COULD BE FLIPPED, ALTHOUGH, IT DOESN'T AFFECT THE RESULT
            let bytesPerRow = (cgimage.bytesPerRow) / (cgimage.width  * cWidth)
            
            let context2: CGContext = CGContext(data: nil, width: cWidth, height: cHeight, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: CGColorSpaceCreateDeviceRGB(), bitmapInfo: cgimage.bitmapInfo.rawValue)!
            
            context2.draw(cgimage, in: cropZone)
            
            let result: CGImage  = context2.makeImage()!
            let finalImage = UIImage(cgImage: result)
            
            return finalImage
            
        }
    }
    
    return nil
}
func loadCover(fileName: String, to: UIImageView) {
//    http://13.230.65.43/Fyp_web/IconBook/0141363905.jpg
    let url = URL.init(string: "http://13.230.65.43/Fyp_web/IconBook/\(fileName)")
//    URLSession.shared.dataTask(with: url) { data, response, error in
//        completion(data, response, error)
//    }.resume()
    if url == nil {
        return
    }
    URLSession.shared.dataTask(with: url!) { (data, response, error) in
        guard let data = data, error == nil else { return }
        print("Download Success: \(data)")
        DispatchQueue.main.async() {
            if let dataImage = UIImage(data: data) {
                to.image = dataImage
            }
        }
    }.resume()
}
func setLanguage(isChinese: Int, syncWithServer: Int, id: Int, type: String) {
    let userDefaults = UserDefaults.standard
    userDefaults.set(isChinese, forKey: "isChinese")
    switch(isChinese) {
    case 0:
        print("English Coming")
        userDefaults.set(["Base", "zh-TW", "en"], forKey: "AppleLanguages")
        break
    case 1:
        print("中文嚟喇")
        userDefaults.set(["zh-TW", "Base", "en"], forKey: "AppleLanguages")
        break
    default:
        print("English Coming")
        userDefaults.set(["Base", "zh-TW", "en"], forKey: "AppleLanguages")
        break
    }
    DispatchQueue.main.async {userDefaults.synchronize()}
    if syncWithServer == 1 {
        let url = URL.init(string: "http://13.230.65.43:3000/api/setischinese")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue(userDefaults.object(forKey: "token") as! String, forHTTPHeaderField: "token")
        request.addValue(String(isChinese), forHTTPHeaderField: "ischinese")
        request.addValue(String(id), forHTTPHeaderField:  "id")
        request.addValue(type, forHTTPHeaderField:  "type")
        performRequest(request: request, completion: {(data, code, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            if let successDic = jsonUnwrap(targetObject: data) as? [String: Any] {
                DispatchQueue.main.async {TTGSnackbar(message: NSLocalizedString("Sync with server success", comment: ""), duration: .middle).show()}
            }
        })
    }
}
extension UIViewController
{
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
}
extension UIImage {
    func preserveAspectRatio() -> UIImage {
//        //Refinement code to allow conversion to NSData or share UIImage. Code here:
//        //http://stackoverflow.com/questions/2240395/uiimage-created-from-cgimageref-fails-with-uiimagepngrepresentation
//        let cgimage: CGImage = (self.cgImage)!
//        let cropZone = CGRect(x: 0, y: 0, width: Int(self.size.width), height: Int(self.size.height))
//        let cWidth: size_t  = size_t(cropZone.size.width)
//        let cHeight: size_t  = size_t(cropZone.size.height)
//        let bitsPerComponent: size_t = cgimage.bitsPerComponent
//        //THE OPERATIONS ORDER COULD BE FLIPPED, ALTHOUGH, IT DOESN'T AFFECT THE RESULT
//        let bytesPerRow = (cgimage.bytesPerRow) / (cgimage.width  * cWidth)
//
//        let context2: CGContext = CGContext(data: nil, width: cWidth, height: cHeight, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: CGColorSpaceCreateDeviceRGB(), bitmapInfo: cgimage.bitmapInfo.rawValue)!
//
//        context2.draw(cgimage, in: cropZone)
//
//        let result: CGImage  = context2.makeImage()!
//        let finalImage = UIImage(cgImage: result)
//        let oldWidth = self.size.width
//        let scaleFactor = scaledToWidth / oldWidth
//
//        let newHeight = sourceImage.size.height * scaleFactor
//        let newWidth = oldWidth * scaleFactor
//
//        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
//        sourceImage.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
//        let newImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        return newImage!
        return self
    }
}
