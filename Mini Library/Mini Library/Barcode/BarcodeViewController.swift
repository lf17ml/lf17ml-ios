//
//  BarcodeViewController.swift
//  Mini Library
//
//  Created by FearLix Yeung on 15/1/2018.
//  Copyright © 2018 lf17ml. All rights reserved.
//

import UIKit
import CoreImage

class BarcodeViewController: UIViewController {
    @IBOutlet weak var barcodeUIImageView: UIImageView!
    @IBOutlet weak var detailUILabel: UILabel!
    var userId = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        Draw barcode
//        let data = "M-\(userId)".data(using: .ascii)
//        let filter = CIFilter(name: "CICode128BarcodeGenerator")
//        filter?.setValue(data, forKey: "inputMessage")
//        barcodeUIImageView.image = UIImage(ciImage: (filter?.outputImage)!)
        barcodeUIImageView.image = generateBarcode(from: "M-\(userId)")
        
        detailUILabel.text = NSLocalizedString("Member ID: ", comment: "DETAIL of memberId") + String(userId)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
