//
//  ReservedStaffViewController.swift
//  Mini Library
//
//  Created by FearLix Yeung on 17/1/2018.
//  Copyright © 2018 lf17ml. All rights reserved.
//

import UIKit

class ReservedStaffViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var mainUITableView: UITableView!
    var staffId: Int = 0
    var reservedISBNs = [String]()
    var reservationIds = [Int]()
    var reservedBookNames = [String]()
    var reservedBookDetails = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let url = URL.init(string: "http://13.230.65.43:3000/api/listreservedbystaffid")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue(String(staffId), forHTTPHeaderField: "id")
        performRequest(request: request, completion: {(data, code, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            if let dic = jsonUnwrap(targetObject: data) as? [String: Any] {
                for reserved in dic["reserved"] as! [[String: Any]] {
                    self.reservedISBNs.append(reserved["iSBN"] as! String)
                    self.reservationIds.append(reserved["reservationId"] as! Int)
                    self.reservedBookNames.append(reserved["bookName"] as! String)
                    self.reservedBookDetails.append(reserved["endDate"] as! String)
                }
                DispatchQueue.main.async {self.mainUITableView.reloadData()}
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //table
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return NSLocalizedString("Reserved Books", comment: "")
        default:
            return nil
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return reservedISBNs.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "reservedStaffTableViewCell", for: indexPath) as? ReservedStaffTableViewCell else {
                fatalError("The dequeued cell is not an instance of the desired cell.")
            }
            cell.titleUILabel.text = reservedBookNames[indexPath.row]
            cell.subtitleUILabel.text = String(reservedISBNs[indexPath.row]) + "(" + reservedBookDetails[indexPath.row] + ")"
            return cell
        default:
            return UITableViewCell()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
