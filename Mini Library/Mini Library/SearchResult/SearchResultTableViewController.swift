//
//  SearchResultTableViewController.swift
//  Mini Library
//
//  Created by FearLix Yeung on 29/1/2018.
//  Copyright © 2018 lf17ml. All rights reserved.
//

import UIKit

class SearchResultTableViewController: UITableViewController {
    var userId:Int = 0
    var parameter = ""
    var iSBNs = [String]()
    var titles = [String]()
    var authors = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        if parameter == "" {
            return
        }

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        let url = URL.init(string: "http://13.230.65.43:3000/api/searchbook")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue(parameter, forHTTPHeaderField: "parameter")
        request.addValue(String(userId), forHTTPHeaderField: "userid")
        performRequest(request: request, completion: {(data, code, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            if let successDic = jsonUnwrap(targetObject: data) as? [String: Any] {
                for book in successDic["result"] as! [[String: Any]] {
                    self.iSBNs.append(book["iSBN"] as! String)
                    self.titles.append(book["bookName"] as! String)
                    self.authors.append(book["author"] as! String)
                }
                DispatchQueue.main.async {self.tableView.reloadData()}
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return iSBNs.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "searchResultTableViewCell", for: indexPath) as? SearchResultTableViewCell else {
            fatalError("The dequeued cell is not an instance of SearchResultTableViewCell.")
        }
        cell.titleUILabel.text = titles[indexPath.row]
        cell.authorUILabel.text = authors[indexPath.row]
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "searchResultSegue" {
            let controller = segue.destination as! BookDetailReserveViewController
            if let indexPath = self.tableView.indexPathForSelectedRow {
                controller.iSBN = iSBNs[indexPath.row]
                controller.userId = self.userId
                controller.bookTitle = titles[indexPath.row]
                controller.author = authors[indexPath.row]
            }
        }
    }

}
