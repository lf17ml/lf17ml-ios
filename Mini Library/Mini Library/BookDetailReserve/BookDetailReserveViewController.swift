//
//  BookDetailReserveViewController.swift
//  Mini Library
//
//  Created by FearLix Yeung on 19/1/2018.
//  Copyright © 2018 lf17ml. All rights reserved.
//

import UIKit
import TTGSnackbar

class BookDetailReserveViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var titleUILabel: UILabel!
    @IBOutlet weak var authorUILabel: UILabel!
    @IBOutlet weak var mainUITableView: UITableView!
    var iSBN = ""
    var bookTitle = ""
    var author = ""
    var userId: Int = 0
    let token = UserDefaults.standard.object(forKey: "token") as! String
    var branchIds = [Int]()
    var branchNames = [String]()
    var branchQuantities = [Int]()
    var selectedIndex: Int = -1
    var alert = UIAlertController()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        titleUILabel.text = bookTitle
        authorUILabel.text = author
        
        let url = URL.init(string: "http://13.230.65.43:3000/api/searchbookdetail")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue(iSBN, forHTTPHeaderField: "isbn")
        request.addValue(String(userId), forHTTPHeaderField: "userid")
        performRequest(request: request, completion: {(data, code, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            if let successDic = jsonUnwrap(targetObject: data) as? [String: Any] {
                for branch in successDic["branches"] as! [[String: Any]] {
                    self.branchIds.append(branch["branchId"] as! Int)
                    self.branchNames.append(branch["branchName"] as! String)
                    self.branchQuantities.append(branch["bookLeft"] as! Int)
                }
            } else {
            }
            DispatchQueue.main.async {self.mainUITableView.reloadData()}
        })
    }
    @IBAction func reserveUIButtonTUI(_ sender: Any) {
        if selectedIndex == -1 {
            return
        }
//        No quantity
        if branchQuantities[selectedIndex] == 0 {
            alert = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString("The branch selected(\(branchNames[selectedIndex])) does not have enough book to reserve.", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .`default`, handler: { _ in
                NSLog("The \"OK\" alert occured.")
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        alert = UIAlertController(title: NSLocalizedString("Confirm?", comment: ""), message: NSLocalizedString("You are going to reserve \(bookTitle) from \(branchNames[selectedIndex])", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .`default`, handler: { _ in
            let url = URL.init(string: "http://13.230.65.43:3000/api/addreservation")
            var request = URLRequest(url: url!)
            request.httpMethod = "GET"
            request.addValue(self.token, forHTTPHeaderField: "token")
            request.addValue(self.iSBN, forHTTPHeaderField: "isbn")
            request.addValue(String(self.userId), forHTTPHeaderField: "userid")
            performRequest(request: request, completion: {(data, code, error) in
                guard let data = data, error == nil else {                                                 // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    return
                }
                if let successDic = jsonUnwrap(targetObject: data) as? [String: Any] {
                    if successDic["result"] != nil {
                        DispatchQueue.main.async {
                            TTGSnackbar(message: NSLocalizedString("SUCCESS: Reservation Added", comment: ""), duration: .middle).show()
                        }
                    }
                } else {
                }
            })
            self.navigationController?.popViewController(animated: true)
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .`default`, handler: { _ in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    table
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return branchIds.count
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "bookDetailReserveTableViewCell", for: indexPath) as? BookDetailReserveTableViewCell else {
                fatalError("The dequeued cell is not an correct instance.")
            }
            cell.titleUILabel.text = branchNames[indexPath.row]
            cell.subTitleUILabel.text = String(branchQuantities[indexPath.row])
            return cell
        default:
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return NSLocalizedString("Reservable Books", comment: "")
        default:
            return nil
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            selectedIndex = indexPath.row
            return
        default:
            return
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
