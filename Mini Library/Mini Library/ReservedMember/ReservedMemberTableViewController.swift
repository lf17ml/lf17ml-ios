//
//  ReservedMemberTableViewController.swift
//  Mini Library
//
//  Created by FearLix Yeung on 3/4/2018.
//  Copyright © 2018 lf17ml. All rights reserved.
//

import UIKit
import TTGSnackbar

class ReservedMemberTableViewController: UITableViewController {
    @IBOutlet var mainUITableView: UITableView!
    var userId: Int = 0
    let token = UserDefaults.standard.object(forKey: "token") as! String
    var reservedISBNs = [String]()
    var reservedTitles = [String]()
    var reservedAuthors = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
         self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        let url = URL.init(string: "http://13.230.65.43:3000/api/listreservedbyuserid")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue(String(userId), forHTTPHeaderField: "id")
        performRequest(request: request, completion: {(data, code, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            if let dic = jsonUnwrap(targetObject: data) as? [String: Any] {
                for reservedBook in dic["reserved"] as! [[String: Any]] {
                    self.reservedISBNs.append(reservedBook["iSBN"] as! String)
                    self.reservedTitles.append(reservedBook["bookName"] as! String)
                    self.reservedAuthors.append(reservedBook["author"] as! String)
                }
                DispatchQueue.main.async {self.mainUITableView.reloadData()}
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return reservedISBNs.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "reservedMemberTableViewCell", for: indexPath) as? ReservedMemberTableViewCell else {
                fatalError("The dequeued cell is not an instance of ReservedMemberTableViewCell.")
            }
            cell.titleUILabel.text = reservedTitles[indexPath.row]
            cell.subtitleUILabel.text = reservedAuthors[indexPath.row]
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return NSLocalizedString("Reserved Books", comment: "")
        default:
            return nil
        }
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            let url = URL.init(string: "http://13.230.65.43:3000/api/removereservation")
            var request = URLRequest(url: url!)
            request.httpMethod = "GET"
            request.addValue(String(userId), forHTTPHeaderField: "userid")
            request.addValue(token, forHTTPHeaderField: "token")
            request.addValue(reservedISBNs[indexPath.row], forHTTPHeaderField: "isbn")
            performRequest(request: request, completion: {(data, code, error) in
                guard let data = data, error == nil else {                                                 // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    return
                }
                if let dic = jsonUnwrap(targetObject: data) as? [String: Any] {
                    if dic["result"] != nil {
                        DispatchQueue.main.async {
                            TTGSnackbar(message: NSLocalizedString("SUCCESS: Reservation Removed", comment: ""), duration: .middle).show()
                        }
                    }
                }
            })
            reservedISBNs.remove(at: indexPath.row)
            reservedTitles.remove(at: indexPath.row)
            reservedAuthors.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
            return
        }    
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "searchResultSegue" {
            let controller = segue.destination as! BookDetailReserveViewController
            if let indexPath = self.tableView.indexPathForSelectedRow {
                controller.iSBN = reservedISBNs[indexPath.row]
                controller.userId = self.userId
                controller.bookTitle = reservedTitles[indexPath.row]
                controller.author = reservedAuthors[indexPath.row]
            }
        }
    }

}
