//
//  SettingMemberTableViewController.swift
//  Mini Library
//
//  Created by FearLix Yeung on 3/4/2018.
//  Copyright © 2018 lf17ml. All rights reserved.
//

import UIKit

class SettingMemberTableViewController: UITableViewController {
    @IBOutlet var languageUITableViewCells: [UITableViewCell]!
    var userId:Int = 0
    var type = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        readLanguage()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        switch section {
        case 0:
            return 2
        case 1:
            return 2
        default:
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("invoke didSelectRowAt")
        switch(indexPath.section) {
        case 0:
            switch(indexPath.row) {
            case 0:
                setLanguage(isChinese: 0, syncWithServer: 1, id: userId, type: type)
                break
            case 1:
                setLanguage(isChinese: 1, syncWithServer: 1, id: userId, type: type)
                break
            default:
                break
            }
        case 1:
            break
        default:
            break
        }
        readLanguage()
    }
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "changePasswordSegue" {
            let controller = segue.destination as! SettingPasswordViewController
            controller.id = self.userId
            controller.type = type
        }
    }
    
    func readLanguage() {
        let userDefaults = UserDefaults.standard
        let isChinese = userDefaults.object(forKey: "isChinese") as! Int
        if(isChinese == 0) {
            DispatchQueue.main.async {
                self.languageUITableViewCells[0].accessoryType = .checkmark
                self.languageUITableViewCells[1].accessoryType = .none
            }
        } else {
            DispatchQueue.main.async {
                self.languageUITableViewCells[0].accessoryType = .none
                self.languageUITableViewCells[1].accessoryType = .checkmark
            }
        }
    }

}
