//
//  BookCoverListTableViewController.swift
//  Mini Library
//
//  Created by FearLix Yeung on 28/3/2018.
//  Copyright © 2018 lf17ml. All rights reserved.
//

import UIKit
import TTGSnackbar

class BookCoverListTableViewController: UITableViewController, BookCoverViewControllerDelegate {
    @IBOutlet var mainUITableView: UITableView!
//    DEV
    var staffId:Int = 0
    var token = ""
    var iSBNs = [String]()
    var bookNames = [String]()
    var authors = [String]()
    var selectedIndex:Int = -1

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        token = UserDefaults.standard.object(forKey: "token") as! String
        
        let url = URL.init(string: "http://13.230.65.43:3000/api/listnoicon")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue(String(staffId), forHTTPHeaderField: "staffid")
        performRequest(request: request, completion: {(data, code, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            if let dic = jsonUnwrap(targetObject: data) as? [String: Any] {
                for book in dic["books"] as! [[String: Any]] {
                    self.iSBNs.append(book["iSBN"] as! String)
                    self.bookNames.append(book["bookName"] as! String)
                    self.authors.append(book["author"] as! String)
                }
                DispatchQueue.main.async {self.mainUITableView.reloadData()}
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
//        return iSBNs.count
        switch section {
        case 0:
            print("iSBNs.count: \(iSBNs.count)")
            return iSBNs.count
        default:
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "bookCoverListTableViewCell", for: indexPath) as? BookCoverListTableViewCell else {
                fatalError("The dequeued cell is not an instance of BookCoverListTableViewCell.")
            }
            cell.titleUILabel.text = bookNames[indexPath.row]
            cell.subtitleUILabel.text = "\(authors[indexPath.row]) \(iSBNs[indexPath.row])"
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            self.selectedIndex = indexPath.row
        default:
            return
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "bookCoverSegue" {
            let controller = segue.destination as! BookCoverViewController
            controller.delegate = self
        }
    }
    func childViewControllerResponse(bookCoverUIImage: UIImage)
    {
//        print("SelectedImageIndex after response: \(selectedIndex)")
//        print("bookCoverUIImage: \(bookCoverUIImage)")
        let imageData = UIImageJPEGRepresentation(bookCoverUIImage, 1)
//        let url = URL.init(string: "http://13.230.65.43:3000/api/updatenoicon")
//        var request = URLRequest(url: url!)
//        request.httpMethod = "POST"
//        request.addValue(token, forHTTPHeaderField: "token")
//        request.addValue(iSBNs[selectedIndex], forHTTPHeaderField: "isbn")
//        request.httpBody = Data(imageData!)
//        performRequest(request: request, completion: {(data, code, error) in
//            guard let data = data, error == nil else {                                                 // check for fundamental networking error
//                print("error=\(String(describing: error))")
//                return
//            }
//            if let dic = jsonUnwrap(targetObject: data) as? [String: Any] {
//                if dic["result"] != nil {
//                    DispatchQueue.main.async {
//                        TTGSnackbar(message: NSLocalizedString("SUCCESS: Book Cover(\(self.iSBNs[self.selectedIndex])) Updated", comment: ""), duration: .middle).show()
//                    }
//                }
//            }
//        })
    }
}
