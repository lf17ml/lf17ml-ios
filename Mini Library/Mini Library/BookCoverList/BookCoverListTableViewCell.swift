//
//  BookCoverListTableViewCell.swift
//  Mini Library
//
//  Created by FearLix Yeung on 28/3/2018.
//  Copyright © 2018 lf17ml. All rights reserved.
//

import UIKit

class BookCoverListTableViewCell: UITableViewCell {
    @IBOutlet weak var titleUILabel: UILabel!
    @IBOutlet weak var subtitleUILabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
