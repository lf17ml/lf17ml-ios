//
//  BookDetailTableViewController.swift
//  Mini Library
//
//  Created by FearLix Yeung on 11/1/2018.
//  Copyright © 2018 lf17ml. All rights reserved.
//

import UIKit

class BookDetailTableViewController: UITableViewController {
    @IBOutlet weak var idUILabel: UILabel!
    @IBOutlet weak var iSBNUILabel: UILabel!
    @IBOutlet weak var authorUILabel: UILabel!
    @IBOutlet weak var publishDateUILabel: UILabel!
    @IBOutlet weak var entryDateUILabel: UILabel!
    @IBOutlet weak var titleUILabel: UILabel!
    @IBOutlet weak var descriptionUILabel: UILabel!
    @IBOutlet weak var deweyUILabel: UILabel!
    @IBOutlet weak var branchIdUILabel: UILabel!
    @IBOutlet weak var positionUILabel: UILabel!
    @IBOutlet weak var ageLimitUILabel: UILabel!
    @IBOutlet weak var reservedUserIdUILabel: UILabel!
    @IBOutlet weak var reservationEndDateUILabel: UILabel!
    @IBOutlet weak var borrowedUserIdUILabel: UILabel!
    var bookId:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        let url = URL.init(string: "http://13.230.65.43:3000/api/getbookbyid")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue(String(self.bookId), forHTTPHeaderField: "id")
        performRequest(request: request, completion: {(data, code, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            if let dic = jsonUnwrap(targetObject: data) as? [String: Any] {
                DispatchQueue.main.async {
                    self.idUILabel.text = String(describing: dic["bookID"] as! Int)
                    self.iSBNUILabel.text = dic["iSBN"] as? String
                    self.authorUILabel.text = dic["author"] as? String
                    self.publishDateUILabel.text = dic["publishDate"] as? String
                    self.entryDateUILabel.text = dic["entryDate"] as? String
                    self.titleUILabel.text = dic["name"] as? String
                    self.descriptionUILabel.text = dic["desription"] as? String
                    if dic["dewey"] as? String != nil {
                        self.deweyUILabel.text = String(describing: dic["dewey"] as! Int)
                    }
                    self.branchIdUILabel.text = String(describing: dic["centerBranchID"] as! Int)
                    self.positionUILabel.text = dic["position"] as? String
                    self.ageLimitUILabel.text = String(describing: dic["ageLimit"] as! Int)
                    if dic["reservedUserId"] as? String != nil {
                        self.reservedUserIdUILabel.text = String(describing: dic["reservedUserId"] as! Int)
                    }
                    self.reservationEndDateUILabel.text = dic["reservationEndDate"] as? String
                    if dic["borrowedUserId"] as? String != nil {
                        self.borrowedUserIdUILabel.text = String(describing: dic["borrowedUserId"] as! Int)
                    }
                }
            } else {
                DispatchQueue.main.async {self.idUILabel.text = NSLocalizedString("ERROR: No Such Book ID", comment: "")}
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 1
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return 0
//    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
