//
//  ReturnViewController.swift
//  Mini Library
//
//  Created by FearLix Yeung on 12/1/2018.
//  Copyright © 2018 lf17ml. All rights reserved.
//

import UIKit
import AVFoundation

class ReturnViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, UITableViewDelegate, UITableViewDataSource  {
    @IBOutlet weak var mainUITableView: UITableView!
    @IBOutlet weak var previewUIView: UIView!
    let captureSession = AVCaptureSession()
    private let supportedCodeTypes = [
        AVMetadataObject.ObjectType.code128 // its the one
        //        AVMetadataObject.ObjectType.qr
    ]
    var previewLayer: AVCaptureVideoPreviewLayer?
    var captureDevice: AVCaptureDevice!
    var qrCodeFrameView: UIView?
    var returningBookIds = [Int]()
    var returningBookNames = [String]()
    var returningBookStatuses = [String]()
    let token = UserDefaults.standard.object(forKey: "token") as! String
    var successBooksWithFine = [Int]()
    var successBooksWithoutFine = [Int]()
    var fine: Int = 0  
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        prepareCamera()
    }

    @IBAction func returnUIButtonTUI(_ sender: Any) {
        var returnStr = ""
        for book in returningBookIds {
            returnStr.append(String(book) + " ")
        }
        let url = URL.init(string: "http://13.230.65.43:3000/api/return")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue(returnStr, forHTTPHeaderField: "bookids")
        request.addValue(token, forHTTPHeaderField:  "token")
        performRequest(request: request, completion: {(data, code, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            if let successDic = jsonUnwrap(targetObject: data) as? [String: Any] {
//                if (successDic["returnedbooks"] as! [Int]).count + (successDic["renewedbooks"] as! [Int]).count == self.borrowingBookIds.count {
//                    DispatchQueue.main.async {
//                        TTGSnackbar(message: NSLocalizedString("SUCCESS: All Book Borrowed", comment: ""), duration: .middle).show()
//                    }
//                } else {
//                    var successBooks = ""
//                    var renewBooks = ""
//                    if successDic["borrowedbooks"] != nil {
//                        successBooks = (successDic["borrowedbooks"] as! [Int]).map(String.init).joined(separator: ", ")
//                    }
//                    if successDic["renewedbooks"] != nil {
//                        renewBooks += (successDic["renewedbooks"] as! [Int]).map(String.init).joined(separator: ", ")
//                    }
//                    let alert = UIAlertController(title: NSLocalizedString("FAIL: Not All Book Borrowed", comment: ""), message: NSLocalizedString("Borrowed Books: \(successBooks)\nRenewed Books: \(renewBooks)", comment: "FAIL: Not All Book Borrowed DETAIL"), preferredStyle: .alert)
//                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .`default`, handler: { _ in
//                        NSLog("The \"OK\" alert occured.")
//                    }))
//                    self.present(alert, animated: true, completion: nil)
//
//                }
                
            }
        })
    }
    
    @IBAction func addBookIdDEOE(_ sender: Any) {
        let senderUITextField = sender as! UITextField
        if let input = Int(senderUITextField.text!) {
            addBook(id: input)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func prepareCamera() {
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        let availableDevices = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .back).devices
        captureDevice = availableDevices.first
        if captureDevice != nil {
            beginSession()
        }
    }
    func beginSession() {
        do {
            //            check permission from user
            let captureDeviceInput = try AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(captureDeviceInput)
        } catch {
            print(error.localizedDescription)
        }
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        let bounds = previewUIView.bounds
        previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        previewLayer?.bounds = bounds
        previewUIView.layer.addSublayer(previewLayer!)
        previewLayer?.frame = previewUIView.layer.frame
        captureSession.startRunning()
        
        //        frame highlighting
        qrCodeFrameView = UIView()
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            previewUIView.addSubview(qrCodeFrameView)
            previewUIView.bringSubview(toFront: qrCodeFrameView)
        }
        
        
        let metaDataOutput = AVCaptureMetadataOutput()
        //        if captureSession.canAddOutput(metaDataOutput) {
        captureSession.addOutput(metaDataOutput)
        //        }
        metaDataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        metaDataOutput.metadataObjectTypes = supportedCodeTypes
        captureSession.commitConfiguration()
    }
    override func viewDidLayoutSubviews() {
        previewLayer?.position = CGPoint(x: previewUIView.frame.width/2, y: previewUIView.frame.height/2)
    }
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        //        print("metadataOutput")
        if metadataObjects.count == 0 {
            //            qrCodeFrameView?.frame = CGRect.zero
            return
        }
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        //        if metadataObj.type == AVMetadataObject.ObjectType.qr
        
        if metadataObj.stringValue != nil {
            //            update qrframe
            let barCodeObject = previewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            let reading = metadataObj.stringValue
            if let subReading = reading?.split(separator: "-", maxSplits: 2) {
                if subReading.count == 2 {
                    if subReading[0] == "B" {
                        if !returningBookIds.contains(Int(subReading[1])!) {
                            addBook(id: Int(subReading[1])!)
                        }
                    }
                }
            }
        }
    }
    func addBook(id: Int) {
        returningBookIds.append(id)
        returningBookNames.append("")
        returningBookStatuses.append("")
        mainUITableView.beginUpdates()
        mainUITableView.insertRows(at: [IndexPath(row: returningBookIds.count - 1, section: 0)], with: .automatic)
        mainUITableView.endUpdates()
        loadBook(id: id, index: returningBookIds.count - 1)
    }
    func loadBook(id: Int, index: Int) {
        let url = URL.init(string: "http://13.230.65.43:3000/api/getbookbyid")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue(String(id), forHTTPHeaderField: "id")
        performRequest(request: request, completion: {(data, code, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            if let bookDic = jsonUnwrap(targetObject: data) as? [String: Any] {
                self.returningBookNames[index] = bookDic["name"] as! String
//                if (bookDic["noBorrow"] as! Int) == 1 {
//                    self.returningBookStatuses[index] = NSLocalizedString("FAIL: Book No Borrow", comment: "")
//                    self.mainUITableView.reloadData()
//                } else if userId != 0 {
//                    //DEV check center
//                    if (bookDic["reservationEndDate"] as! String).jSStringToDate() > Date.init() && id == bookDic["reservedUserId"] as! Int {
//                        self.returningBookStatuses[index] = NSLocalizedString("SUCCESS: Reserver Borrowing", comment: "")
//                        self.mainUITableView.reloadData()
//                    } else if (bookDic["borrowedUserId"] as! Int) != userId && userId != 0 {
//                        self.returningBookStatuses[index] = NSLocalizedString("FAIL: UserId And BorrowingUserId Mismatch", comment: "")
//                        self.mainUITableView.reloadData()
//                    } else {
//                        self.returningBookStatuses[index] = NSLocalizedString("SUCCESS: Book Can Be Returned", comment: "")
//                        self.mainUITableView.reloadData()
//                    }
//                }
                self.mainUITableView.reloadData()
            } else {
                self.returningBookNames[index] = NSLocalizedString("ERROR: No Such Book ID", comment: "")
                self.mainUITableView.reloadData()
            }
        })
    }
    //    table
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return returningBookIds.count
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "returnTableViewCell", for: indexPath) as? ReturnTableViewCell else {
                fatalError("The dequeued cell is not an instance of ReturnBookTableViewCell.")
            }
            cell.titleUILabel.text = returningBookNames[indexPath.row]
            cell.subtitleUILabel.text = String(returningBookIds[indexPath.row]) + " " + returningBookStatuses[indexPath.row]
            return cell
        default:
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return NSLocalizedString("Returning Books", comment: "")
        default:
            return nil
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            //            let sb = UIStoryboard(name: "Main", bundle: Bundle.main)
            //            let nextViewController = sb.instantiateViewController(withIdentifier: "BookDetailViewController") as! BookDetailViewController
            ////            nextViewController.id = id
            //            //DEV: temporary used, animation wrong
            //            self.present(nextViewController, animated: true, completion: nil)
            return
        default:
            return
        }
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "returning" {
            let controller = segue.destination as! BookDetailTableViewController
            if let indexPath = self.mainUITableView.indexPathForSelectedRow {
                if indexPath.section == 1 {
                    controller.bookId = returningBookIds[indexPath.row]
                } else {
                    print("ERROR: prepare segue indexPath.section not equals 1")
                }
            }
        }
        if segue.identifier == "returnedDetailSegue" {
            
        }
    }

}
