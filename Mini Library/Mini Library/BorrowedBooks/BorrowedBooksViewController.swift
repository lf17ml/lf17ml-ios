//
//  BorrowedBooksViewController.swift
//  
//
//  Created by FearLix Yeung on 13/1/2018.
//

import UIKit

class BorrowedBooksViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var mainUITableView: UITableView!
    var userId:Int = 0
    var unreturnedBookIds = [Int]()
    var unreturnedBookNames = [String]()
    var unreturnedBookDues = [String]()
    var returnedBookIds = [Int]()
    var returnedBookNames = [String]()
    var returnedBookBorrowDates = [String]()
    var returnedBookPayStates = [Int]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let url = URL.init(string: "http://13.230.65.43:3000/api/showborrowhistorybyuserid")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue(String(userId), forHTTPHeaderField: "id")
        performRequest(request: request, completion: {(data, code, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            if let dic = jsonUnwrap(targetObject: data) as? [String: Any] {
                for unreturnedBook in dic["unreturnedbooks"] as! [[String: Any]] {
                    self.unreturnedBookIds.append(unreturnedBook["bookId"] as! Int)
                    self.unreturnedBookNames.append(unreturnedBook["bookName"] as! String)
                    self.unreturnedBookDues.append(unreturnedBook["dueDate"] as! String)
                }
                for returnedBook in dic["returnedbooks"] as! [[String: Any]] {
                    self.returnedBookIds.append(returnedBook["bookId"] as! Int)
                    self.returnedBookNames.append(returnedBook["bookName"] as! String)
                    self.returnedBookBorrowDates.append(returnedBook["borrowDate"] as! String)
                    self.returnedBookPayStates.append(returnedBook["payState"] as! Int)
                }
                DispatchQueue.main.async {self.mainUITableView.reloadData()}
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //table
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return NSLocalizedString("Unreturned Books", comment: "")
        case 1:
            return NSLocalizedString("Returned Books", comment: "")
        default:
            return nil
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return unreturnedBookIds.count
        case 1:
            return returnedBookIds.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "borrowedBooksTableViewCell", for: indexPath) as? BorrowedBooksTableViewCell else {
                fatalError("The dequeued cell is not an instance of BorrowBookTableViewCell.")
            }
            cell.titleUILabel.text = unreturnedBookNames[indexPath.row]
            cell.subtitleUILabel.text = String(unreturnedBookIds[indexPath.row]) + " due on " + unreturnedBookDues[indexPath.row]
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "borrowedBooksTableViewCell", for: indexPath) as? BorrowedBooksTableViewCell else {
                fatalError("The dequeued cell is not an instance of BorrowBookTableViewCell.")
            }
            cell.titleUILabel.text = returnedBookNames[indexPath.row]
            cell.subtitleUILabel.text = String(returnedBookIds[indexPath.row]) + " borrowed on " + returnedBookBorrowDates[indexPath.row]
            return cell
        default:
            return UITableViewCell()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
