//
//  HistoryViewController.swift
//  Mini Library
//
//  Created by FearLix Yeung on 18/1/2018.
//  Copyright © 2018 lf17ml. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {
    var userId: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "borrowedMemberSegue" {
            let controller = segue.destination as! BorrowedBooksViewController
            controller.userId = self.userId
        }
        if segue.identifier == "reservedMemberSegue" {
            let controller = segue.destination as! ReservedMemberTableViewController
            controller.userId = self.userId
        }
    }

}
