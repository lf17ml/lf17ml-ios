//
//  HomeStaffViewController.swift
//  Mini Library
//
//  Created by FearLix Yeung on 19/12/2017.
//  Copyright © 2017 lf17ml. All rights reserved.
//

import UIKit

class HomeStaffViewController: UIViewController {
    var staffId: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "reservedStaffSegue" {
            let controller = segue.destination as! ReservedStaffViewController
            controller.staffId = self.staffId
        } else if segue.identifier == "bookCoverListSegue" {
            let controller = segue.destination as! BookCoverListTableViewController
            controller.staffId = self.staffId
        } else if segue.identifier == "settingSegue" {
            let controller = segue.destination as! SettingMemberTableViewController
            controller.type = "S"
            controller.userId = self.staffId
        }
    }

}
