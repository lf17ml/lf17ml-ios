//
//  SearchViewController.swift
//  Mini Library
//
//  Created by FearLix Yeung on 18/1/2018.
//  Copyright © 2018 lf17ml. All rights reserved.
//

import UIKit
import TTGSnackbar

class SearchViewController: UIViewController {
    @IBOutlet weak var searchUIButton: UIButton!
    @IBOutlet weak var barcodeUIButton: UIButton!
    @IBOutlet weak var searchUITextField: UITextField!
    @IBOutlet var topBooksUIImageViews: [UIImageView]!
    @IBOutlet var topBooksTitleUILabels: [UILabel]!
    @IBOutlet var topBooksAuthorUILabels: [UILabel]!
    @IBOutlet var newBooksUIImageViews: [UIImageView]!
    @IBOutlet var newBooksTitleUILabels: [UILabel]!
    @IBOutlet var newBooksAuthorUILabels: [UILabel]!
    var searchBarInputTemp = ""
    var userId: Int = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()

        // Do any additional setup after loading the view.
        searchUIButton.setImage(UIImage(named: "search"), for: UIControlState.normal)
        barcodeUIButton.setImage(UIImage(named: "barcode"), for: UIControlState.normal)
        
        let url = URL.init(string: "http://13.230.65.43:3000/api/topbooks")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue(String(userId), forHTTPHeaderField: "userid")
        performRequest(request: request, completion: {(data, code, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            if let books = (jsonUnwrap(targetObject: data) as! [String: Any])["books"] as? [[String: Any]] {
                for (i, book) in books.enumerated() {
                    if !(book["icon"] is NSNull) {
                        loadCover(fileName: "\(book["iSBN"] as! String).\(book["icon"] as! String)", to: self.topBooksUIImageViews[i])
                    }
                    DispatchQueue.main.async {
                        self.topBooksTitleUILabels[i].text = book["bookName"] as? String
                        self.topBooksAuthorUILabels[i].text = book["author"] as? String

                    }
                }
            }
        })
        let url2 = URL.init(string: "http://13.230.65.43:3000/api/newbooks")
        var request2 = URLRequest(url: url2!)
        request2.httpMethod = "GET"
        request2.addValue(String(userId), forHTTPHeaderField: "userid")
        performRequest(request: request2, completion: {(data, code, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            if let books = (jsonUnwrap(targetObject: data) as! [String: Any])["books"] as? [[String: Any]] {
                for (i, book) in books.enumerated() {
                    if !(book["icon"] is NSNull) {
                        loadCover(fileName: "\(book["iSBN"] as! String).\(book["icon"] as! String)", to: self.newBooksUIImageViews[i])
                    }
                    DispatchQueue.main.async {
                        self.newBooksTitleUILabels[i].text = book["bookName"] as? String
                        self.newBooksAuthorUILabels[i].text = book["author"] as? String
                        
                    }
                }
            }
        })
//        for i in 0...9 {
//            if topBooksTitleUILabels.count > i {
//                topBooksTitleUILabels[i].text = "bookName(\(i))"
//            }
//            if topBooksAuthorUILabels.count > i {
//                topBooksAuthorUILabels[i].text = "author(\(i))"
//            }
//            if newBooksTitleUILabels.count > i {
//                newBooksTitleUILabels[i].text = "bookName2(\(i))"
//            }
//            if newBooksAuthorUILabels.count > i {
//                newBooksAuthorUILabels[i].text = "author2(\(i))"
//            }
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        searchUITextField.text = searchBarInputTemp
        searchBarInputTemp = ""
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "searchResultSegue" {
            let controller = segue.destination as! SearchResultTableViewController
            controller.parameter = searchUITextField.text!
            controller.userId = self.userId
        }
        
    }

}
