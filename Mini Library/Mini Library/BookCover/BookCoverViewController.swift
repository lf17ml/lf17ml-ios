//
//  BookCoverViewController.swift
//  Mini Library
//
//  Created by FearLix Yeung on 29/3/2018.
//  Copyright © 2018 lf17ml. All rights reserved.
//

import UIKit

class BookCoverViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBOutlet weak var mainUIImageView: UIImageView!
    var imagePicker = UIImagePickerController()
    var output = UIImage()
    var delegate: BookCoverViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func cameraTUI(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
//            imagePicker.allowsEditing = true
            present(imagePicker, animated: true, completion: nil)
        }
    }
    @IBAction func photoLibraryTUI(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
//            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    @IBAction func doneTUI(_ sender: Any) {
        self.delegate?.childViewControllerResponse(bookCoverUIImage: self.output)
        self.navigationController?.popViewController(animated: true)
    }
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
//        let imageView = info[UIImagePickerControllerEditedImage] as! UIImage
        output = info[UIImagePickerControllerOriginalImage] as! UIImage
        mainUIImageView.image = output
        dismiss(animated:true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
protocol BookCoverViewControllerDelegate
{
    func childViewControllerResponse(bookCoverUIImage: UIImage)
}
