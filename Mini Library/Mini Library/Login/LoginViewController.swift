//
//  LoginViewController.swift
//  Mini Library
//
//  Created by FearLix Yeung on 14/12/2017.
//  Copyright © 2017 lf17ml. All rights reserved.
//

import UIKit
import Firebase
import LocalAuthentication

class LoginViewController: UIViewController {
    @IBOutlet weak var performUIButton: UIButton!
    @IBOutlet weak var emailUITextField: UITextField!
    @IBOutlet weak var passwordUITextField: UITextField!
    var id: Int = 0
    var type = ""
    var validity: Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.hideKeyboard()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroud1")!)
//        Load userDefaults
        let userDefaults = UserDefaults.standard
        if let uDId = userDefaults.object(forKey: "id") as? Int {
            let uDType = userDefaults.object(forKey: "type") as! String
            let uDToken = userDefaults.object(forKey: "token") as! String
            print("uDType: \(uDType)")
            print("uDToken: \(uDToken)")
//            switch(uDType) {
//                case "M":
//                    DispatchQueue.main.async {
//                        self.performSegue(withIdentifier: "memberLoginSegue", sender: nil)
//                    }
//                    break
//                case "S":
//                    DispatchQueue.main.async {
//                        self.performSegue(withIdentifier: "staffLoginSegue", sender: nil)
//                    }
//                    break
//                default:
//
//                    break
//            }
//            validate login
            let url = URL.init(string: "http://13.230.65.43:3000/api/verifylogin")
            var request = URLRequest(url: url!)
            request.httpMethod = "GET"
            request.addValue(uDToken, forHTTPHeaderField: "token")
            performRequest(request: request, completion: {(data, code, error) in
                guard let data = data, error == nil else {                                                 // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    self.validity = 0
                    return
                }
                if let successDic = jsonUnwrap(targetObject: data) as? [String: Any] {
                    if successDic.isEmpty {
                        print("Cannot varify login")
                        self.validity = 0
                        return
                    }
                    //                Login successful
                    self.id = uDId
                    self.type = uDType
                    self.validity = 1
                } else {
                    //                fail DEV
                }
            })
//            Activate TouchID or FaceID or else
            let authenticationContext = LAContext()
            var error:NSError?
            // Check if the device has a fingerprint sensor
            guard authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
                print("ERROR: No biomatic sensor is detected")
                return
            }
            authenticationContext.evaluatePolicy(
                .deviceOwnerAuthenticationWithBiometrics,
                localizedReason: "Login to previous logged user",
                reply: { [unowned self] (success, error) -> Void in
                    
                    if( success ) {
                        // Fingerprint recognized
                        print("Fingerprint recognized")
                        print("self.validity: \(self.validity)")
                        if self.validity == 1 {
                            switch(self.type) {
                            case "M":
                                DispatchQueue.main.async {
                                    self.performSegue(withIdentifier: "memberLoginSegue", sender: nil)
                                }
                                break
                            case "S":
                                DispatchQueue.main.async {
                                    self.performSegue(withIdentifier: "staffLoginSegue", sender: nil)
                                }
                                break
                            default:
                                
                                break
                            }
                        }
                    } else {
                        if self.validity == -1 {
                            return
                        }
                        // Check if there is an error
                        if let error = error {
//                            switch errorCode {
//
//                            case LAError.AppCancel.rawValue:
//                                message = "Authentication was cancelled by application"
//
//                            case LAError.AuthenticationFailed.rawValue:
//                                message = "The user failed to provide valid credentials"
//
//                            case LAError.InvalidContext.rawValue:
//                                message = "The context is invalid"
//
//                            case LAError.PasscodeNotSet.rawValue:
//                                message = "Passcode is not set on the device"
//
//                            case LAError.SystemCancel.rawValue:
//                                message = "Authentication was cancelled by the system"
//
//                            case LAError.TouchIDLockout.rawValue:
//                                message = "Too many failed attempts."
//
//                            case LAError.TouchIDNotAvailable.rawValue:
//                                message = "TouchID is not available on the device"
//
//                            case LAError.UserCancel.rawValue:
//                                message = "The user did cancel"
//
//                            case LAError.UserFallback.rawValue:
//                                message = "The user chose to use the fallback"
//
//                            default:
//                                message = "Did not find error code on LAError object"
//
//                            }
                            print("authenticationContext.evaloatePolicy() error: \(error)")
                            return
                        }
                        let alert = UIAlertController(title: NSLocalizedString("Cannot varify login", comment: ""), message: NSLocalizedString("Please login again.", comment: ""), preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                        self.present(alert, animated: true)
                    }
                    
            })
        }
    }
    @IBAction func performUIButtonTUI(_ sender: UIButton) {
        let url = URL.init(string: "http://13.230.65.43:3000/api/login")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.addValue(emailUITextField.text!, forHTTPHeaderField: "email")
        request.addValue(passwordUITextField.text!, forHTTPHeaderField: "password")
        request.addValue(Messaging.messaging().fcmToken!, forHTTPHeaderField: "iostoken")
        performRequest(request: request, completion: {(data, code, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            if let userIdDic = jsonUnwrap(targetObject: data) as? [String: Any] {
                if userIdDic.isEmpty {
                    print("Cannot login as member")
                    return
                }
//                Login successful
                self.id = userIdDic["id"] as! Int
                self.type = userIdDic["type"] as! String
                let isChinese = userIdDic["isChinese"] as! Int
//                make the app into chinese
                if isChinese == 1 {
                    setLanguage(isChinese: 1, syncWithServer: 0, id: 0, type: "")
                }
                let userDefaults = UserDefaults.standard
                userDefaults.set(self.id, forKey: "id")
                userDefaults.set(self.type, forKey: "type")
                userDefaults.set(userIdDic["token"] as! String, forKey: "token")
                userDefaults.set(isChinese, forKey: "isChinese")
                switch(self.type) {
                    case "M":
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "memberLoginSegue", sender: nil)
                        }
                        break
                    case "S":
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "staffLoginSegue", sender: nil)
                        }
                        break
                    default:
                        
                        break
                }
            } else {
//                fail DEV
            }
        })
    }
    @IBAction func MemberTUI(_ sender: Any) {
        emailUITextField.text = "m1@a.a"
        passwordUITextField.text = "Abc1234567890"
    }
    @IBAction func StaffTUI(_ sender: Any) {
        emailUITextField.text = "s1@a.a"
        passwordUITextField.text = "Abc1234567890"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        print("prepare for segue")
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "memberLoginSegue" {
            let controller = (segue.destination as! UINavigationController).topViewController as! TabViewController
            controller.userId = self.id
        } else if segue.identifier == "staffLoginSegue" {
            let controller = (segue.destination as! UINavigationController).topViewController as! HomeStaffViewController
            controller.staffId = self.id
        }
    }

}
