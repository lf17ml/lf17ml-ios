//
//  SearchCameraViewController.swift
//  Mini Library
//
//  Created by FearLix Yeung on 19/1/2018.
//  Copyright © 2018 lf17ml. All rights reserved.
//

import UIKit
import AVFoundation

class SearchCameraViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    @IBOutlet weak var previewUIView: UIView!
    @IBOutlet weak var mainUITableView: UITableView!
    
    var readings = [String]()
    var decidedReading = ""
    let captureSession = AVCaptureSession()
    private let supportedCodeTypes = [
        AVMetadataObject.ObjectType.upce,
        AVMetadataObject.ObjectType.code39,
        AVMetadataObject.ObjectType.code39Mod43,
        AVMetadataObject.ObjectType.code93,
        AVMetadataObject.ObjectType.code128,
        AVMetadataObject.ObjectType.ean8,
        AVMetadataObject.ObjectType.ean13,
        AVMetadataObject.ObjectType.aztec,
        AVMetadataObject.ObjectType.pdf417,
        AVMetadataObject.ObjectType.itf14,
        AVMetadataObject.ObjectType.dataMatrix,
        AVMetadataObject.ObjectType.interleaved2of5,
        AVMetadataObject.ObjectType.qr
    ]
    var previewLayer: AVCaptureVideoPreviewLayer?
    var captureDevice: AVCaptureDevice!
    var qrCodeFrameView: UIView?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        prepareCamera()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func prepareCamera() {
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        let availableDevices = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .back).devices
        captureDevice = availableDevices.first
        if captureDevice != nil {
            beginSession()
        }
    }
    func beginSession() {
        do {
            //            check permission from user
            let captureDeviceInput = try AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(captureDeviceInput)
        } catch {
            print(error.localizedDescription)
        }
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        let bounds = previewUIView.bounds
        previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        previewLayer?.bounds = bounds
        previewUIView.layer.addSublayer(previewLayer!)
        previewLayer?.frame = previewUIView.layer.frame
        captureSession.startRunning()
        
        //        frame highlighting
        qrCodeFrameView = UIView()
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            previewUIView.addSubview(qrCodeFrameView)
            previewUIView.bringSubview(toFront: qrCodeFrameView)
        }
        
        
        let metaDataOutput = AVCaptureMetadataOutput()
        //        if captureSession.canAddOutput(metaDataOutput) {
        captureSession.addOutput(metaDataOutput)
        //        }
        metaDataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        metaDataOutput.metadataObjectTypes = supportedCodeTypes
        captureSession.commitConfiguration()
    }
    override func viewDidLayoutSubviews() {
        previewLayer?.position = CGPoint(x: previewUIView.frame.width/2, y: previewUIView.frame.height/2)
    }
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        //        print("metadataOutput")
        if metadataObjects.count == 0 {
            //            qrCodeFrameView?.frame = CGRect.zero
            return
        }
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        //        if metadataObj.type == AVMetadataObject.ObjectType.qr
        
        if metadataObj.stringValue != nil {
            //            update qrframe
            let barCodeObject = previewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            let reading = metadataObj.stringValue
            if let previousViewController = navigationController?.viewControllers[(navigationController?.viewControllers.count)! - 2] as! TabViewController? {
                previousViewController.searchBarInputTemp = String(reading!)
                navigationController?.popViewController(animated: true)
            }
        }
    }
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//        if segue.identifier == "borrowingSegue" {
//            let controller = segue.destination as! BookDetailTableViewController
//            if let indexPath = self.borrowBookUITableView.indexPathForSelectedRow {
//                if indexPath.section == 1 {
//                    controller.bookId = borrowingBookIds[indexPath.row]
//                } else {
//                    print("ERROR: prepare segue indexPath.section not equals 1")
//                }
//            }
//        }
//    }

}
